/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStyleSheet = ({
  breakpoints,
}) => ({
  form: {
    width: '100%',
  },
  formRow: {
    display: 'flex',
    'flex-wrap': 'wrap',
    '& + $formRow': {
      marginTop: '28px',
    },
  },
  colLeft: {
    flex: 1,
    marginRight: '20px',
    [breakpoints.up('lg')]: {
      flex: 'none',
      width: '50%',
    },
  },
  colRight: {
    width: '30px',
    'align-self': 'center',
  },
  label: {
    flex: '0 0 100%',
    marginBottom: '6px',
  },
});
