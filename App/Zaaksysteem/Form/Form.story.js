import {
  React,
  stories,
  boolean,
} from '../../story';
import Form from '.';

const DEFAULT_ERROR_MSG = 'Voer een geldige waarde in.';
const choices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const [, defaultChoice] = choices;
const stores = {
  Default: {
    select: defaultChoice,
    text: 'Default text',
    creatable: choices,
    html: 'test <strong>value</strong> in story 😀',
    checkbox: true,
  },
};

stories(module, __dirname, {
  Default({ store }) {

    const set = keyValuePairs => {
      store.set(keyValuePairs);
    };

    const fieldSets = [
      {
        title: 'Form story with description',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        fields: [
          {
            choices,
            constraints: ['required'],
            label: 'Select test',
            name: 'select',
            promises: () => [
              import('react-select'),
            ],
            type: 'select',
            value: store.state.select,
            help: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis erat in nisi bibendum, sed commodo nisi varius.',
            externalLabel: true,
          },
          {
            type: 'text',
            label: 'Test label 1',
            name: 'text',
            isMulti: false,
            disabled: false,
            reference: '1',
            required: false,
            value: store.state.text,
            constraints: ['uri'],
            help: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis erat in nisi bibendum, sed commodo nisi varius.',
          },
          {
            type: 'creatable',
            label: 'Creatable select',
            name: 'creatable',
            isMulti: true,
            disabled: false,
            promises: () => [
              import('react-select/lib/Creatable'),
              import('react-select'),
            ],
            required: false,
            value: store.state.creatable,
            constraints: ['required'],
            help: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis erat in nisi bibendum, sed commodo nisi varius.',
            externalLabel: true,
          },
        ],
      },
      {
        title: 'Form story',
        fields: [
          {
            constraints: ['required'],
            label: 'HTML editor',
            name: 'html',
            type: 'html',
            value: store.state.html,
            help: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis erat in nisi bibendum, sed commodo nisi varius.',
            externalLabel: true,
          },
          {
            constraints: ['required'],
            name: 'checkbox',
            type: 'checkbox',
            value: store.state.checkbox,
          },
        ],
      },
    ];

    const identifier = `FormStory-${errors}`;
    const errors = boolean('Error(s)', false);

    function validate() {
      return errors ? DEFAULT_ERROR_MSG : undefined;
    }

    return (
      <Form
        fieldSets={fieldSets}
        set={set}
        validate={validate}
        changed={false}
        identifier={identifier}
        onErrors={() => {}}
      />
    );
  },
}, stores);
