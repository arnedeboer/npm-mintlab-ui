import React, { Component, createElement, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { bind, reduceMap } from '@mintlab/kitchen-sink';
import equals from 'fast-deep-equal';
import Render from '../../Abstract/Render';
import Card from '../../Material/Card';
import * as typeMap from './library/typemap';
import TooltipWrapper from './library/TooltipWrapper';
import InfoIcon from './library/InfoIcon';
import { formStyleSheet } from './Form.style';

const { assign } = Object;

/**
 * Composite Form component.
 *
 * Depends on {@link Card} and {@link Render}
 *
 * @see  /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Form
 * @see /npm-mintlab-ui/documentation/consumer/manual/Form.html
 *
 * @reactProps {Array<Object>} fieldSets
 *   Form control configurations, grouped, with an optional title and description
 * @reactProps {Function} set
 *   Set a form control's model value in the external state manager
 * @reactProps {Function} validate
 *   Validate a form control value literal
 * @reactProps {string} identifier
 *   String used to generate a unique key for every form field
 * @reactProps {Object} classes
 * @reactProps {Function} onErrors
 */
export class Form extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const errors = this.getStateFromFields();

    this.state = {
      errors,
    };

    this.props.onErrors(errors);
    this.setExternalState();
    bind(this, 'onBlur', 'onFocus', 'onChange');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if (!equals(prevProps.fieldSets, this.props.fieldSets)) {
      const errors = this.getStateFromFields();

      this.setState({
        errors,
      });

      this.props.onErrors(errors);

      if (!equals(prevProps.identifier, this.props.identifier)) {
        this.setExternalState();
      }
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      getFieldSet,
      props: {
        classes,
        fieldSets,
      },
      onBlur,
      onFocus,
      onChange,
    } = this;

    return (
      <Fragment>
        <div
          onChange={onChange}
          onBlur={onBlur}
          onFocus={onFocus}
          className={classes.form}
        >
          {fieldSets && fieldSets.map(getFieldSet, this)}
        </div>
      </Fragment>
    );
  }

  // Custom methods:

  /**
  * Called whenever a form component loses focus.
  * This method is also passed as a prop to every form field component.
  */
  onBlur() {
    this.setState({
      focus: undefined,
    });
  }

  /**
  * Called whenever a form component gains focus.
  * This method is also passed as a prop to every form field component.
  * @param {Object} props
  * @param {Object} props.target optional wrapper for props
  * @param {string} props.name
  */
  onFocus(props) {
    const {
      name,
    } = this.normalizeEventProps(props);

    this.setState({
      focus: name,
    });
  }

  /**
   * Process a field's onChange event.
   * Call the external state manager with the new new key/value pair after
   * normalizing the value.
   * This method is also passed as a prop to every form field component.
   *
   * @param {Object} props
   * @param {Object} props.target optional wrapper for props
   * @param {boolean} props.checked
   * @param {string} props.name
   * @param {string} props.type
   * @param {*} props.value
   */
  onChange(props) {
    const {
      checked,
      name,
      type,
      value,
    } = this.normalizeEventProps(props);

    const field = this.getFieldByName(name);

    if (!field) return;

    const {
      constraints,
      config,
    } = field;

    const normalizedValue = this.getNormalizedFormValue({
      checked,
      type,
      value,
      constraints,
      config,
    });

    this.props.set({
      [name]: normalizedValue,
    });
  }

  /**
   * @param {Object} fieldSet
   * @param {*} index
   * @return {ReactElement}
   */
  getFieldSet({
    title,
    description,
    fields,
  }, index) {
    const {
      props: {
        identifier,
      },
      getFormControl,
    } = this;

    return (
      <Fragment
        key={`${identifier}-${index}`}
      >
        <Card
          title={title}
          description={description}
        >
          {fields.map(getFormControl, this)}
        </Card>
      </Fragment>
    );
  }

  /**
   * @param {Object} props
   * @param {string} [props.constraints=[]]
   * @param {string} props.name
   * @param {string} props.type
   * @param {string} [props.value]
   * @param {string} props.label
   * @param {boolean} props.externalLabel
   * @return {ReactElement}
   */
  getFormControl({
    constraints = [],
    name,
    type,
    value,
    help,
    label,
    externalLabel,
    ...rest
  }) {
    const {
      state: {
        errors,
        focus,
      },
      props: {
        identifier,
        classes,
      },
      onChange,
      onBlur,
      onFocus,
    } = this;

    const error = errors[name];
    const required = constraints.includes('required');
    const hasFocus = focus === name;

    return (
      <div
        key={`${identifier}-${name}`}
        className={classes.formRow}
      >

        <Render condition={externalLabel}>
          <div className={classes.label}>
            { label }
          </div>
        </Render>

        <div className={classes.colLeft}>
          <TooltipWrapper
            condition={Boolean(error && !hasFocus)}
            title={error}
          >
            {createElement(typeMap[type], {
              // ZS-FIXME: add generic normalization
              checked: (typeof value === 'boolean') ? value : undefined,
              hasFocus,
              error,
              name,
              onFocus,
              onBlur,
              onChange,
              required,
              label,
              value,
              ...rest,
            })}
          </TooltipWrapper>
        </div>

        <div className={classes.colRight}>
          <Render condition={Boolean(help)}>
            <TooltipWrapper
              title={help}
              type='info'
            >
              <InfoIcon/>
            </TooltipWrapper>
          </Render>
        </div>

      </div>
    );
  }

  /**
   * Get a flattened array of all fields in the fieldset
   *
   * @param {Object} fieldSet
   * @return {Array}
   */
  getFieldList(fieldSet) {
    if (fieldSet) {
      const reduceFields = (accumulator, { fields }) =>
        accumulator.concat(fields);

      return fieldSet.reduce(reduceFields, []);
    }

    return [];
  }

  /**
   * @param {string} needle
   * @return {Object}
   */
  getFieldByName(needle) {
    const { fieldSets } = this.props;
    const findField = ({ name }) => name === needle;

    return this
      .getFieldList(fieldSets)
      .find(findField);
  }

  /**
   * @param {Object} options
   * @param {boolean} options.checked
   * @param {string} options.type
   * @param {string} options.value
   * @param {Array<string>} options.constraints
   * @return {boolean|number|string}
   */
  getNormalizedFormValue({
    checked,
    type,
    value,
    constraints,
  }) {

    const map = new Map([
      [() => type === 'checkbox', () => checked],
      [() => constraints && constraints.includes('number'), () => {
        const valueToNumber = Number(value);
        return Number.isNaN(valueToNumber) ? value : valueToNumber;
      }],
    ]);

    return reduceMap({
      map,
      fallback: value,
    });
  }

  /**
   * Get the form's submission state.
   *
   * @return {Object}
   */
  getStateFromFields() {
    const { fieldSets, validate } = this.props;

    const reduceErrors = (accumulator, { constraints, name, value, config }) => {
      const error = validate(value, constraints, config);

      if (error) {
        accumulator[name] = error;
      }

      return accumulator;
    };

    const errors = this
      .getFieldList(fieldSets)
      .reduce(reduceErrors, {});

    return errors;
  }

  /**
   * Save all key/value pairs to the external state manager.
   */
  setExternalState() {
    const { fieldSets, set } = this.props;
    const values = this
      .getFieldList(fieldSets)
      .reduce((accumulator, { name, value }) =>
        assign(accumulator, {
          [name]: value,
        }), {});

    set(values);
  }


  /**
  * @param {Object} props
  * @return {Object}
  */
  normalizeEventProps(props) {
    return props.target || props;
  }
}

export default withStyles(formStyleSheet)(Form);
