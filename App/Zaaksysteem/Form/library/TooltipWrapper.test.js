import React from 'react';
import { mount } from 'enzyme';
import TooltipWrapper from './TooltipWrapper';

xdescribe('The `TooltipWrapper` component', () => {
  test('renders its children in a tooltip if `condition` is truthy (default)', () => {
    const wrapper = mount(
      <TooltipWrapper
        title="Fubar"
      >FUBAR</TooltipWrapper>
    );

    const actual = wrapper.exists('WithStyles(Tooltip)');
    const expected = true;

    expect(actual).toBe(expected);
  });

  test('renders its children if condition is falsy', () => {
    const Child = () => (<hr/>);
    const wrapper = mount(
      <TooltipWrapper
        condition={false}
        title="Fubar"
      ><Child/></TooltipWrapper>
    );

    const actual = [
      wrapper.exists('WithStyles(Tooltip)'),
      wrapper.exists('Child'),
    ];
    const expected = [false, true];

    expect(actual).toEqual(expected);
  });
});
