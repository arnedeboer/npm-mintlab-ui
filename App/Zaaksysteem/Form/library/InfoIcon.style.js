const GREY_WEIGHT = 400;

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const infoIconStyleSheet = ({
  palette: {
    primary,
    grey,
  },
}) => ({
  root: {
    color: grey[GREY_WEIGHT],
    fontSize: '20px',
    '&:hover': {
      color: primary.main,
    },
  },
});
