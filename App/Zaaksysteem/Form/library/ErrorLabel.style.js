/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the ErrorLabel component
 * @param {Object} theme
 * @return {JSS}
 */
export const errorLabelStyleSheet = ({
  palette: {
    error,
  },
}) => ({
  root: {
    color: error.main,
  },
});
