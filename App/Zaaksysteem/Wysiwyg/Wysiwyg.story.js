import {
  React,
  stories,
  boolean,
  text,
} from '../../story';
import Editor from '.';

const stores = {
  Default: {
    value: 'Hey this editor rocks 😀',
  },
};

stories(module, __dirname, {
  Default({ store }) {

    const onChange = ({
      value,
    }) => {
      store.set({
        value,
      });
    };

    return (
      <Editor
        name='editor'
        value={store.state.value}
        readOnly={boolean('Read-only', false)}
        error={text('Error', '')}
        hasFocus={boolean('Focus', false)}
        onChange={onChange}
      />
    );
  },
}, stores);
