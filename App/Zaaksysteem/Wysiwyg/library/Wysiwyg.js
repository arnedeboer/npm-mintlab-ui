import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { bind, callOrNothingAtAll, cloneWithout } from '@mintlab/kitchen-sink';
import equals from 'fast-deep-equal';
import Render from '../../../Abstract/Render';
import ErrorLabel from '../../Form/library/ErrorLabel';
import { wysiwygStyleSheet } from './Wysiwyg.style';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

/**
 * Rich text editor facade for *React Draft Wysiwyg*.
 * - additional props are passed through to that component
 *
 * Dependencies {@link Render}
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Wysiwyg
 * @see /npm-mintlab-ui/documentation/consumer/manual/Wysiwyg.html
 * @see https://jpuri.github.io/react-draft-wysiwyg/#/docs
 *
 * @reactProps {boolean} disabled
 * @reactProps {string} [error]
 * @reactProps {string} name
 * @reactProps {Function} [onChange]
 * @reactProps {Function} [onBlur]
 * @reactProps {Function} [onFocus]
 * @reactProps {string} [value='']
 */
export class Wysiwyg extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      toolbar: {
        options: ['inline'],
      },
      value: '',
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    this.state = {
      editorState: this.valueToEditorState(props.value),
    };

    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * If the incoming value from props is different than that
   * of the internal state, reset the editor with the value
   * from props. This handles cases where the value needs to
   * be forcefully reset.
   *
   * @param {Object} prevProps
   * @param {Object} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    const {
      valueToEditorState,
      getEditorValue,
      props: {
        value,
      },
      state: {
        editorState,
      },
    } = this;

    if (
      equals(editorState, prevState.editorState) &&
      !equals(getEditorValue(editorState), value)
    ) {
      this.setState({
        editorState: valueToEditorState(value),
      });
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        classes: {
          defaultClass,
          errorClass,
          editorClass,
        },
        disabled,
        hasFocus,
        ...rest
      },
      state: {
        editorState,
      },
      handleBlur,
      handleChange,
      handleFocus,
    } = this;

    return (
      <div>
        <Editor
          editorState={editorState}
          onEditorStateChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          wrapperClassName={classNames(
            defaultClass, {
              [errorClass]: error,
            })}
          toolbarClassName={classNames(
            defaultClass, {
              [errorClass]: error,
            })}
          editorClassName={classNames(
            defaultClass,
            editorClass, {
              [errorClass]: error,
            })}
          readOnly={disabled}
          {...cloneWithout(rest, 'onBlur', 'onChange', 'onFocus', 'value')}
        />
        <Render condition={Boolean(hasFocus && error)}>
          <ErrorLabel label={error} />
        </Render>
      </div>
    );
  }

  // Custom methods:

  /**
   * @param {*} value
   * @return {*}
   */
  valueToEditorState(value) {
    const contentBlock = htmlToDraft(value);

    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);

      return editorState;
    }
  }

  /**
  * @param {Object} editorState
  * @return {Object}
  */
  getEditorValue(editorState) {
    return draftToHtml(convertToRaw(editorState.getCurrentContent()));
  }

  /**
   * @param {Object} editorState
   */
  handleChange(editorState) {
    const value = this.getEditorValue(editorState);

    const {
      name,
      onChange,
    } = this.props;

    this.setState({
      editorState,
    });

    callOrNothingAtAll(onChange, () => [{
      name,
      value,
      type: 'html',
    }]
    );
  }

  /**
   * @param {Event} event
   */
  handleBlur(event) {
    const { onBlur } = this.props;

    event.stopPropagation();

    callOrNothingAtAll(onBlur, () => []);
  }

  /**
   * @param {Event} event
   */
  handleFocus(event) {
    const { onFocus, name } = this.props;

    event.stopPropagation();

    callOrNothingAtAll(onFocus, () => [{
      target: {
        name,
      },
    }]);
  }
}

export default withStyles(wysiwygStyleSheet)(Wysiwyg);
