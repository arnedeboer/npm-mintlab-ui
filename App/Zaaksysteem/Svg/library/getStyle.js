import { getStyleFromHeight } from './getStyleFromHeight';
import { getStyleFromWidth } from './getStyleFromWidth';

// ZS-FIXME: cyclomatic complexity
/* eslint complexity: [error, 6] */

/**
 * `Svg` component utility function.
 *
 * @param {Array} viewBox
 * @param {string} [width] CSS length or percentage
 * @param {string} [height] CSS length or percentage
 * @param {string} [className]
 * @return {Object|null}
 */
export function getStyle(viewBox, width, height, className) {
  // 1: Set `width` and `height` as is.
  if (width && height) {
    return {
      width,
      height,
    };
  }

  // 2: Calculate the style object based on  `width`.
  if (width) {
    return getStyleFromWidth(viewBox, width);
  }

  // 3: Calculate the style object based on `height`.
  if (height) {
    return getStyleFromHeight(viewBox, height);
  }

  // 4: Delegate sizing responsibility to `className`.
  if (className) {
    return null;
  }

  // 5: Use the viewBox width/height as CSS pixel lengths.
  const [viewBoxWidth, viewBoxHeight] = viewBox;

  return {
    width: `${viewBoxWidth}px`,
    height: `${viewBoxHeight}px`,
  };
}
