
import React from 'react';
import Divider from '@material-ui/core/Divider';
import Map from '../../../Abstract/Map';
import DropdownMenuButton from './DropdownMenuButton';
import { dropdownMenuListStylesheet } from './DropdownMenuList.style';
import { withStyles } from '@material-ui/core/styles';

const { isArray } = Array;

/**
 * Returns a wrapper with one or more groups of DropdownMenuButton components.
 * Groups are seperated by a Divider component.
 * 
 * @param {Array<Action>|Array<Array<Action>>} items
 * @param {Object} classes
 * @return {ReactElement}
 */
const DropdownMenuList = ({
  items,
  classes,
}) => {
  const normalized = items.every(item => isArray(item)) ? items : [items];

  return (
    normalized.map((group, index) => (
      <div
        key={index}
        className={classes.list}
      >
        {Boolean(index) && index !== normalized.length &&
          <Divider />
        }
        <Map
          data={group}
          component={DropdownMenuButton}
        />
      </div>
    ))
  );
};

export default withStyles(dropdownMenuListStylesheet)(DropdownMenuList);
