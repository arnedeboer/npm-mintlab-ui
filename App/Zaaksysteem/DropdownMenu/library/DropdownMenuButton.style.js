import { ICON_SELECTOR } from '../../../Material/MaterialUiThemeProvider/library/theme';

/**
 * Style Sheet for the {@link DropdownMenuButton} component
 * 
 * @param {Object} theme
 * @return {JSS}
 */
export const dropdownMenuButtonStylesheet = ({
  palette: {
    primary,
  },
  typography,
  mintlab: {
    greyscale,
    radius,
  },
}) => ({
  root: {
    borderRadius: radius.dropdownButton,
    color: greyscale.offBlack,
    '&:hover': {
      backgroundColor: `${greyscale.offBlack}0D`,
      color: primary.main,
    },
    height: '36px',
    padding: '6px 20px 6px 11px',
  },
  label: {
    'justify-content': 'flex-start',
    padding: '0px',
    [ICON_SELECTOR]: {
      marginRight: '18px',
    },
    fontWeight: typography.fontWeightRegular,
  },
});
