/**
 * Style Sheet for the {@link DropdownMenu} component
 * 
 * @param {Object} theme
 * @return {JSS}
 */
export const DropdownMenuStylesheet = ({
  mintlab: {
    radius,
    shadows,
  },
}) => ({
  paper: {
    borderRadius: radius.dropdownMenu,
    padding: '8px',
    boxShadow: shadows.medium,
  },
  list: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0px',
    margin: '0px',
  },
});
