import React from 'react';
import { iterator } from '../library/iterator';
import style from './Wave.css';

const LENGTH = 5;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Wave = ({ color }) => (
  <div className={style['wave']}>
    {iterator(LENGTH).map((item => (
      <div
        key={item}
        className={style['rect']}
        style={{
          backgroundColor: color,
        }}
      />
    )))}
  </div>
);
