import React, { Fragment } from 'react';
import MuiButton from '@material-ui/core/Button';
import Icon from '../../../Material/Icon';
import { VerticalButtonStylesheet } from './VerticalMenuButton.style';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

/**
 * @param {Action} props
 * @param {Object} props.classes
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {boolean} props.active
 * @return {ReactElement}
 */
export const VerticalMenuButton = ({
  classes,
  action,
  icon,
  label,
  active,
}) => {
  const children = icon ? (
    <Fragment>
      <Icon
        size='small'
        classes={{
          root: classes.icon,
        }}>{icon}</Icon>
      {label}
    </Fragment>
  ) : label;

  return (
    <div>
      <MuiButton
        onClick={action}
        classes={{
          root: classNames(classes.root, {
            [classes.active]: active,
          }),
          label: classes.label,
        }}
      >{children}</MuiButton>
    </div>
  );
};

export default withStyles(VerticalButtonStylesheet)(VerticalMenuButton);
