# 🔌 `VerticalMenu` component

> Vertical menu component.

## See also

- [`VerticalMenu` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/VerticalMenu)
- [`VerticalMenu` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-verticalmenu)
