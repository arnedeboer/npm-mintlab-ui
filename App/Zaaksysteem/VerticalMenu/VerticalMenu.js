import React from 'react';
import Map from '../../Abstract/Map';
import VerticalMenuButton from './library/VerticalMenuButton';
import { VerticalMenuStylesheet } from './VerticalMenu.style';
import { withStyles } from '@material-ui/core/styles';

/**
 * Vertical menu component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/VerticalMenu
 * @see /npm-mintlab-ui/documentation/consumer/manual/VerticalMenu.html
 *
 * @param {*} props
 * @param {Array<Action>} props.items
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const VerticalMenu = ({ 
  items,
  classes,
}) => (
  <nav className={classes.menu}>
    <Map
      data={items}
      component={VerticalMenuButton}
    />
  </nav>
);

export default withStyles(VerticalMenuStylesheet)(VerticalMenu);
