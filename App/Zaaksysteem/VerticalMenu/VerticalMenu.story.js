import {
  React,
  stories,
} from '../../story';
import VerticalMenu from '.';

stories(module, __dirname, {
  Default() {
    const data = [
      { label: 'Foo' },
      { label: 'Bar' },
    ];

    return (
      <VerticalMenu
        items={data}
      />
    );
  },
  Icons() {
    const data = [
      {
        icon: 'menu',
        label: 'Foo',
        active: true,
      },
      {
        icon: 'star',
        label: 'Bar',
      },
      {
        icon: 'home',
        label: 'Baz',
      },
    ];

    return (
      <VerticalMenu
        items={data}
      />
    );
  },
});
