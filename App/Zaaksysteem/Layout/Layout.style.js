/**
 * Style Sheet with theme access for the {@link Layout} component.
 * See the CSS file for the autoprefixed CSS grid.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const layoutStyleSheet = theme => ({
  root: {
    background: theme.mintlab.greyscale.lighter,
  },
  content: {},
});
