import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBar from './library/AppBar';
import PermanentDrawer from './library/PermanentDrawer/PermanentDrawer';
import TemporaryDrawer from './library/TemporaryDrawer/TemporaryDrawer';
import Banners from './library/Banners/Banners';
import { layoutStyleSheet } from './Layout.style';
import styleSheet from './Layout.css';

const DEFAULT_INDEX = 0;

/**
 * Global Layout component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Layout
 * @see /npm-mintlab-ui/documentation/consumer/manual/Layout.html
 *
 * @param {Object} props
 * @param {number} [props.active=0]
 * @param {*} props.children
 * @param {Object} props.classes
 * @param {string} props.customer
 * @param {Object} props.drawer
 * @param {Array} props.drawer.primary
 *    Array of Actions.
 * @param {Array} [props.drawer.secondary]
 *    Array of Actions.
 * @param {Object} [props.drawer.about]
 *    Single Action.
 * @param {string} props.identity
 * @param {boolean} props.isDrawerOpen
 * @param {string} props.menuLabel
 * @param {Function} props.toggleDrawer
 * @param {string} props.userLabel
 * @param {Array} props.userActions
 * @param {Object} props.banners
 * @return {ReactElement}
 */
export const Layout = ({
  active = DEFAULT_INDEX,
  children,
  classes,
  customer,
  drawer,
  identity,
  isDrawerOpen,
  menuLabel,
  toggleDrawer,
  userLabel,
  userName,
  userActions,
  banners,
}) => (
  <div
    className={classNames(styleSheet.Layout, classes.root)}
  >
    <AppBar
      className={styleSheet.AppBar}
      gutter={2}
      menuLabel={menuLabel}
      onMenuClick={toggleDrawer}
      position="absolute"
      userLabel={userLabel}
      userName={userName}
      userActions={userActions}
    >{identity}</AppBar>

    <Banners
      className={styleSheet.Banners}
      banners={banners}
    />

    <PermanentDrawer
      active={active}
      className={styleSheet.PermanentDrawer}
      navigation={drawer.primary}
    />

    <TemporaryDrawer
      active={active}
      navigation={drawer}
      open={isDrawerOpen}
      subtitle={customer}
      title={identity}
      toggle={toggleDrawer}
    />

    <div
      className={classNames(styleSheet.Content, classes.content)}
    >{children}</div>
  </div>
);

export default withStyles(layoutStyleSheet)(Layout);
