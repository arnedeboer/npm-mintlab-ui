/**
 * Style Sheet for the MenuGroup component
 * 
 * @param {Object} theme
 * @return {JSS}
 */
export const menuGroupStylesheet = ({
  palette,
  mintlab: {
    greyscale,
  },
}) => ({
  menuList: {
    '& > *:not(:last-child)': {
      marginBottom: '10px',
    },
  },
  root: {
    color: greyscale.darkest,
    '&:hover': {
      color: palette.primary.main,
    },
    '$active&:hover': {
      backgroundColor: `${palette.primary.main}0D`,
    },
    ':not($active)&:hover': {
      backgroundColor: `${greyscale.offBlack}0D`,
    },
  },
  active: {
    color: palette.primary.main,
    backgroundColor: `${palette.primary.main}0D`,
  },
  inherit: {
    color: 'inherit',
  },
});
