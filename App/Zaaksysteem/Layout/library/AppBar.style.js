/**
 * Style Sheet for the AppBar component
 * @return {JSS}
 */
export const appBarStyleSheet = ({
  breakpoints,
}) => ({
  appBar: {
    boxShadow: 'none',
  },
  content: {
    flexGrow: 1,
    padding: '0px 6px',
  },
  userName: {
    padding: '8px 8px 4px 8px',
  },
  toolbar: {
    [breakpoints.up('xs')]: {
      minHeight: '48px',
    },
  },
});
