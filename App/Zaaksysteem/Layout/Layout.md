# 🔌 `Layout` component

> Global Layout component.

## The `banner` prop

Passed to an internal component that renders one or more Banners. Banners must be provided as an object, where the keys represents a banner's identifier, and the value is a valid set of Banner props.

### Example

```
{
  'secondary': {
    label: 'Fusce auctor, orci eget convallis tincidunt.',
    variant: 'secondary',
    primary: {
      action: action('Primary action'),
      label: 'Primary label',
    },
    secondary: {
      action: action('Secondary action'),
      icon: 'close',
    },
  },
  'review': {
    label: 'Nunc placerat nec nibh bibendum dignissim.',
    variant: 'review',
    primary: {
      action: action('Primary action'),
      label: 'Primary label',
    },
    secondary: [{
      action: action('Secondary action'),
      label: 'First item',
      icon: 'search',
    },
    {
      action: action('Secondary action'),
      label: 'Second item',
      icon: 'favorite',
    }],
  },
};
```

## See also

- [`Layout` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Layout)
- [`Layout` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-layout)
