import {
  React,
  stories,
  select,
  text,
  action,
} from '../../story';
import Banner from '.';

stories(module, __dirname, {
  Default() {
    const map = {
      single: {
        action: action('Secondary action'),
        icon: 'close',
      },
      multiple: [{
        action: action('Secondary action 1'),
        label: 'Secondary action 1',
        icon: 'search',
      },
      {
        action: action('Secondary action 2'),
        label: 'Secondary action 2',
        icon: 'favorite',
      },
      {
        action: action('Secondary action 3'),
        label: 'Secondary action 3',
        icon: 'card_giftcard',
      }],
    };

    const type = select('Secondary type', ['single', 'multiple'], 'single');

    return (
      <div style={{
        width: '100%',
      }}>
        <Banner
          variant={select('Variant', ['primary', 'secondary', 'review', 'danger'], 'secondary')}
          label={text('Label', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')}
          primary={{
            action: action('Primary action'),
            label: 'Pellentesque nec facilisiss',
          }}
          secondary={map[type]}
        />
      </div>
    );
  },
});
