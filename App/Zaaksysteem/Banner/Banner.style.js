import secondary from './svg/secondary.svg';
import danger from './svg/danger.svg';

/**
 * Style Sheet for the {@link Banner} Banner component
 * @param {Object} theme
 * @return {JSS}
 */
export const bannerStylesheet = ({
  palette,
  mintlab: {
    shadows,
    greyscale,
  },
}) => ({
  outer: {
    height: '36px',
    boxShadow: shadows.insetTop,
  },
  inner: {
    display: 'flex',
    height: '100%',
    'background-repeat': 'repeat',
    'justify-content': 'flex-end',
    'align-items': 'center',
    padding: '0px 16px',
    '&>*:not(:last-child)': {
      marginRight: '10px',
    },
  },
  label: {
    color: greyscale.black,
  },
  'outer-primary': {
    backgroundImage: `linear-gradient(to right, ${palette.primary.lighter}, ${palette.primary.support})`,
  },
  'outer-secondary': {
    backgroundImage: `linear-gradient(to right, ${palette.secondary.lighter}, ${palette.secondary.support})`,
  },
  'outer-review': {
    backgroundImage: `linear-gradient(to right, ${palette.review.light}, ${palette.review.support})`,
  },
  'outer-danger': {
    backgroundImage: `linear-gradient(to right, ${palette.danger.light}, ${palette.danger.support})`,
  },
  'inner-primary': {},
  'inner-secondary': {
    'background-size': '70px 70px',
    backgroundImage: `url(${secondary})`,
  },
  'inner-review': {},
  'inner-danger': {
    'background-size': '12px 12px',
    backgroundImage: `url(${danger})`,
  },
});
