import { reduceMap } from '@mintlab/kitchen-sink';

/**
 * @param {Object} options
 * @param {Object} options.state
 * @param {Object} options.primary
 * @param {Object} options.common
 * @param {Object} options.greyscale
 * @return {string}
 */
const backgroundColorOption = ({
  state,
  primary,
  common,
  greyscale,
}) => {
  const map = new Map([
    [() => state.isSelected, () => greyscale.light],
    [() => state.isFocused, () => primary.light],
  ]);
  return reduceMap({
    map,
    fallback: common.white,
  });
};

export default backgroundColorOption;
