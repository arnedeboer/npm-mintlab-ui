/**
 * Custom filter function that also filters on alternative labels
 * @param {Object} option
 * @param {string} input
 * @return {boolean}
 */
const filterOption = (option, input) => {
  if (!input) {
    return true;
  }

  const { label, alternativeLabels } = option.data;
  const labels = [label]
    .concat(alternativeLabels)
    .filter(item => item);
  const clean = value =>
    value
      .trim()
      .toLowerCase();

  return labels.some(thisLabel =>
    clean(thisLabel).includes(clean(input))
  );
};

export default filterOption;
