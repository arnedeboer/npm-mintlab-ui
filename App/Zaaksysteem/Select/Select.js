import { createElement } from 'react';
import LazyLoader from '../../Abstract/LazyLoader';

/**
 * Create a single `react-select` based chunk.
 * Choosing the actual variant is delegated to `SelectStrategy`.
 *
 * Depends on {@link LazyLoader}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const Select = props =>
  createElement(LazyLoader, {
    promise: () => import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.select" */
      './library/SelectStrategy'),
    ...props,
  });

export default Select;
