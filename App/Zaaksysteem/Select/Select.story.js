import {
  React,
  stories,
  text,
  boolean,
} from '../../story';
import Icon from '../../Material/Icon';
import Select from '.';

const DELAY = 2500;

const choices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    alternativeLabels: ['Yummy', 'I love this flavor'],
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const choicesCreatable = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const [, defaultValue] = choices;

const stores = {
  Form: {
    value: defaultValue,
  },
  Generic: {
    value: null,
    choices: [],
  },
  Creatable: {
    value: choicesCreatable,
  },
};

/**
 * @param {SyntheticEvent} event 
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({ value }, store) => store.set({ value });

stories(module, __dirname, {
  Form({ store }) {

    return (
      <div
        style={{
          width: '15rem',
        }}
      >
        <Select
          choices={choices}
          error={text('Error', '')}
          isMulti={boolean('Multiple choices', false)}
          value={store.state.value}
          hasFocus={boolean('Focus', false)}
          onChange={event => onChange(event, store)}
          name='story'
          isClearable={boolean('Clearable', true)}
        />
      </div>
    );
  },
  Generic({ store }) {

    const startAdornment = boolean('Start adornment', true) ? <Icon size='small'>people</Icon> : null;

    return (
      <div
        style={{
          width: '15rem',
        }}
      >
        <Select
          generic={true}
          choices={store.state.choices}
          autoLoad={true}
          getChoices={() => {
            setTimeout(() => {
              store.set({
                choices,
              });
            }, DELAY);
          }}
          value={store.state.value}
          hasFocus={boolean('Focus', false)}
          onChange={event => onChange(event, store)}
          name='story'
          startAdornment={startAdornment}
          isClearable={boolean('Clearable', true)}
        />
      </div>
    );
  },
  Creatable({ store }) {

    return (
      <div
        style={{
          width: '30rem',
        }}
      >
        <Select
          creatable={true}
          error={text('Error', '')}
          value={store.state.value}
          hasFocus={boolean('Focus', false)}
          onChange={event => onChange(event, store)}
          name='story'
        />
      </div>
    );
  },
}, stores);
