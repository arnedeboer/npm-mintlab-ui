import React, { Fragment, createElement } from 'react';
import { withTheme } from '@material-ui/core/styles';
import ReactSelect from 'react-select';
import { bind, cloneWithout } from '@mintlab/kitchen-sink';
import ErrorLabel from '../../Form/library/ErrorLabel';
import Render from '../../../Abstract/Render/Render';
import selectStyleSheet from './Shared.style';
import SelectBase from '../Select.base';
import ClearIndicator from '../library/ClearIndicator';
import DropdownIndicator from '../library/DropdownIndicator';

const { assign } = Object;

/**
 * Choices and value must be provided in the form of a single, or array of objects.
 *
 * @typedef {Object} SelectValue
 * @property {string} value
 * @property {string} label
 * @property {Array<string>} alternativeLabels
 * @example
 * {
 *   value: 'strawberry',
 *   label: 'Strawberry',
 *   alternativeLabels: ['Fruit', 'Red'],
 * }
 */

/**
 * Facade for React Select v2.
 * - additional props are passed through to that component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Select
 * @see /npm-mintlab-ui/documentation/consumer/manual/Select.html
 * @see https://react-select.com/home
 *
 * @reactProps {boolean} [autoLoad=false]
 * @reactProps {SelectValue|Array<SelectValue>} choices
 * @reactProps {boolean} [disabled=false]
 * @reactProps {Function} filterOption
 * @reactProps {Function} getChoices
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Function} onFocus
 * @reactProps {Object} styles
 * @reactProps {Object} translations
 * @reactProps {boolean} hasFocus
 * @reactProps {SelectValue|Array<SelectValue>} value
 */
export class Select extends SelectBase {

  constructor(props) {
    super(props);
    bind(this, 'DropdownWrapper');
  }

  // ZS-FIXME: cyclomatic complexity
  /* eslint complexity: [2, 7] */

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        choices: options,
        error,
        translations,
        theme,
        disabled,
        autoLoad,
        styles,
        hasInitialChoices,
        value,
        hasFocus,
        filterOption,
        ...rest
      },
      state: {
        isLoading,
      },
      handleInputChange,
      handleFocus,
      handleChange,
      handleBlur,
      handleKeyDown,
      DropdownWrapper,
    } = this;

    return (
      <Fragment>
        <ReactSelect
          isDisabled={disabled}
          cacheOptions={true}
          isLoading={(options && !options.length) ? false : isLoading}
          value={value}
          options={options}
          loadingMessage={() => translations['form:loading']}
          noOptionsMessage={() => null}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onKeyDown={handleKeyDown}
          onInputChange={!autoLoad && !hasInitialChoices && handleInputChange}
          placeholder={translations['form:choose']}
          styles={styles || selectStyleSheet({
            theme,
            error,
            hasFocus,
          })}
          classNamePrefix='react-select'
          name={name}
          filterOption={filterOption}
          components={{
            ClearIndicator,
            DropdownIndicator: DropdownWrapper,
          }}
          {...cloneWithout(rest, 'name', 'value', 'onBlur', 'onChange', 'onFocus', 'onKeyDown')}
        />
        <Render
          condition={Boolean(error && hasFocus)}
        >
          <ErrorLabel label={error}/>
        </Render>
      </Fragment>
    );
  }

  /**
   *
   * @param {Object} selectProps
   * @return {ReactElement}
   */
  DropdownWrapper(selectProps) {
    const {
      getChoices,
      options,
    } = this.props;

    return createElement(DropdownIndicator, assign({}, selectProps, { getChoices, options } ));
  }

}

export default withTheme()(Select);
