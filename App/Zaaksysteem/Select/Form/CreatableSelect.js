import React, { Component, Fragment } from 'react';
import { withTheme } from '@material-ui/core/styles';
import ReactSelectCreatable from 'react-select/lib/Creatable';
import { bind, callOrNothingAtAll, cloneWithout } from '@mintlab/kitchen-sink';
import ErrorLabel from '../../Form/library/ErrorLabel';
import Render from '../../../Abstract/Render/Render';
import ClearIndicator from '../library/ClearIndicator';
import selectStyleSheet from './Shared.style';

const MSG_CREATABLE = 'Typ, en <ENTER> om te bevestigen.';
const MSG_CREATE = 'Aanmaken:';

/**
 * Facade for Creatable React Select v2.
 * - additional props will be passed to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/CreatableSelect
 * @see /npm-mintlab-ui/documentation/consumer/manual/CreatableSelect.html
 * @see https://react-select.com/home
 *
 * @reactProps {string} error
 * @reactProps {Function} filterOption
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Function} onFocus
 * @reactProps {Object} translations
 * @reactProps {Object|Array} value
 * @reactProps {Object} styles
 * @reactProps {Object} components
 */

export class CreatableSelect extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:creatable': MSG_CREATABLE,
        'form:create': MSG_CREATE,
      },
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        translations,
        theme,
        styles,
        disabled,
        hasFocus,
        value,
        filterOption,
        ...rest
      },
      handleChange,
      handleBlur,
      handleFocus,
    } = this;

    const createLabel = input =>
      [translations['form:create'], ' ', input].join('');

    return (
      <Fragment>
        <ReactSelectCreatable
          isDisabled={disabled}
          isMulti={true}
          components={{
            DropdownIndicator: null,
            ClearIndicator,
          }}
          value={value}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          placeholder={translations['form:creatable']}
          formatCreateLabel={createLabel}
          noOptionsMessage={() => null}
          styles={styles || selectStyleSheet({
            theme,
            error,
            hasFocus,
          })}
          filterOption={filterOption}
          {...cloneWithout(rest, 'components', 'config', 'name', 'value', 'onBlur', 'onChange', 'onFocus', 'onBlur')}
        />
        <Render
          condition={Boolean(error && hasFocus)}
        >
          <ErrorLabel
            label={error}
          />
        </Render>
      </Fragment>
    );
  }

  // Custom methods:

  /**
   * Call form change function, if provided
   * @param {*} value
   */
  handleChange(value) {
    const { onChange, name } = this.props;

    callOrNothingAtAll(onChange, () => [{
      name,
      value,
      type: 'creatable',
    }]);
  }

  /**
   * Call form blur function, if provided
   * @param {Event} event
   */
  handleBlur(event) {
    const {
      props: { onBlur },
    } = this;

    event.stopPropagation();

    callOrNothingAtAll(onBlur, () => []);
  }

  /**
   * Call form focus function, if provided
   * @param {Event} event
   */
  handleFocus(event) {
    const { props: { onFocus, name } } = this;
    event.stopPropagation();

    callOrNothingAtAll(onFocus, () => [{
      name,
    }]);
  }

}

export default withTheme()(CreatableSelect);
