import { Select, MIN_CHARACTERS } from './Select';
import { createEvent, render, spyOn } from '../../test';

const { assign } = Object;

const select = props => render(Select, assign({
  classes: {},
  theme: null,
}, props));

/**
 * @test {Select}
 */
xdescribe('The `Select` component', () => {
  describe('has a `componentDidMount` lifecycle method', () => {
    test('that calls the `getChoices` method with the empty string if the `autoLoad` prop is truthy', () => {
      const { props } = select({
        autoLoad: true,
        getChoices: jest.fn(),
      });

      const assertedCallCount = 1;
      const assertedArgument = '';

      spyOn(props.getChoices, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `componentDidUpdate` lifecycle method', () => {
    test('that sets the `isLoading` state to false if the choices prop has changed', () => {
      const { instance } = select({
        choices: [{
          foo: 'foo',
        }],
      });

      instance.setState = jest.fn();
      instance.componentDidUpdate({
        foo: 'bar',
      });

      const assertedCallCount = 1;
      const assertedArgument = {
        isLoading: false,
      };

      spyOn(instance.setState, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `handleChange` method', () => {
    test('calls the `onChange` prop with the asserted argument', () => {
      const name = 'FOOBAR';
      const value = {
        FOO: 'BAR',
      };
      const { instance, props } = select({
        name,
        onChange: jest.fn(),
      });

      instance.handleChange(value);

      const assertedCallCount = 1;
      const assertedArgument = {
        target: {
          name,
          value,
          type: 'select',
        },
      };

      spyOn(props.onChange, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `handleBlur` method', () => {
    test('that stops event propagation', () => {
      const { instance } = select();

      const event = createEvent('blur');
      const assertedCallCount = 1;

      instance.handleBlur(event);

      spyOn(event.stopPropagation, assertedCallCount);
    });

    test('sets the `hasFocus` state to `false`', () => {
      const { instance } = select();
      const event = createEvent('blur');

      instance.setState = jest.fn();
      instance.handleBlur(event);

      const assertedCallCount = 1;
      const assertedArgument = {
        hasFocus: false,
      };

      spyOn(instance.setState, assertedCallCount, assertedArgument);
    });

    test('that calls the `onBlur` prop', () => {
      const name = 'FOO';
      const { instance, props } = select({
        name,
        onBlur: jest.fn(),
      });
      const event = createEvent('blur');

      instance.handleBlur(event);

      const assertedCallCount = 1;
      const assertedArgument = {
        target: {
          name,
          type: 'select',
          value: instance.state.selected,
        },
      };

      spyOn(props.onBlur, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `handleFocus` method', () => {
    test('that stops event propagation', () => {
      const { instance } = select();
      const event = createEvent('focus');

      instance.handleFocus(event);

      const assertedCallCount = 1;

      spyOn(event.stopPropagation, assertedCallCount);
    });

    test('that sets the `hasFocus` state to `true`', () => {
      const { instance } = select();
      const event = createEvent('blur');

      instance.setState = jest.fn();
      instance.handleFocus(event);

      const assertedCallCount = 1;
      const assertedArgument = {
        hasFocus: true,
      };

      spyOn(instance.setState, assertedCallCount, assertedArgument);
    });

    test('that calls the `onFocus` prop', () => {
      const name = 'FOO';
      const { instance, props } = select({
        name,
        onFocus: jest.fn(),
      });
      const event = createEvent('focus');

      instance.handleFocus(event);

      const assertedCallCount = 1;
      const assertedArgument = {
        target: {
          name,
        },
      };

      spyOn(props.onFocus, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `handleInputChange` method', () => {
    test('does nothing if the input argument is not valid', () => {
      const { instance } = select();

      const offset = 1;
      const valueUnderTest = 'x'.repeat(MIN_CHARACTERS - offset);

      instance.handleInputChange(valueUnderTest);

      const actual = instance.timeoutId;
      const asserted = null;

      expect(actual).toBe(asserted);
    });

    test('clears an existing timeout', () => {
      const { instance } = select();
      const input = 'x'.repeat(MIN_CHARACTERS);
      const id = 42;

      instance.timeoutId = id;
      global.clearTimeout = jest.fn();
      instance.handleInputChange(input);

      const assertedCallCount = 1;
      const assertedArgument = id;

      spyOn(global.clearTimeout, assertedCallCount, assertedArgument);
    });

    test('calls the `getChoices` method with a timeout if input is valid', done => {
      const timeoutId = setTimeout();
      const { instance } = select({
        getChoices: jest.fn(() => {
          const actual = (instance.timeoutId !== timeoutId);
          const asserted = true;

          expect(actual).toBe(asserted);
          done();
        }),
      });
      const input = 'x'.repeat(MIN_CHARACTERS);

      instance.timeoutId = timeoutId;
      instance.handleInputChange(input);
    });
  });

  describe('has an `isValidInput` method that returns', () => {
    describe('`false` if', () => {
      test('its argument is falsy', () => {
        const { instance } = select();

        const actual = instance.isValidInput();
        const asserted = false;

        expect(actual).toBe(asserted);
      });

      test(`its argument’s length is < ${MIN_CHARACTERS}`, () => {
        const { instance } = select();
        const offset = 1;
        const valueUnderTest = 'x'.repeat(MIN_CHARACTERS - offset);

        const actual = instance.isValidInput(valueUnderTest);
        const asserted = false;

        expect(actual).toBe(asserted);
      });
    });

    test(`\`true\` if its argument’s length is >= ${MIN_CHARACTERS}`, () => {
      const { instance } = select();
      const valueUnderTest = 'x'.repeat(MIN_CHARACTERS);

      const actual = instance.isValidInput(valueUnderTest);
      const asserted = true;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `getChoices` method', () => {
    test('that does not set state if no `getChoices` prop is passed', () => {
      const { instance } = select();

      instance.getChoices();

      const actual = instance.state.isLoading;
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('that sets the loading state and calls the `getChoices` prop if it is passed', () => {
      const { instance, props } = select({
        getChoices: jest.fn(),
      });
      const input = 'FOOBAR';

      instance.getChoices(input);

      const actual = instance.state.isLoading;
      const asserted = true;

      expect(actual).toBe(asserted);

      const assertedCallCount = 1;
      const assertedArgument = input;

      spyOn(props.getChoices, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `filterOption` method that returns', () => {
    describe('`true` if the second argument is', () => {
      test('a case-insensitive substring of a label', () => {
        const { instance } = select();

        const actual = instance.filterOption({
          data: {
            label: 'myFooBar',
            alternativeLabels: [],
          },
        }, 'foo');
        const asserted = true;

        expect(actual).toBe(asserted);
      });

      test('a case-insensitive substring of an alternative label', () => {
        const { instance } = select();

        const actual = instance.filterOption({
          data: {
            label: 'myQuuxBar',
            alternativeLabels: [
              'myFooBar',
            ],
          },
        }, 'foo');
        const asserted = true;

        expect(actual).toBe(asserted);
      });
    });

    describe('`false` if the second argument is', () => {
      test('falsy', () => {
        const { instance } = select();

        const actual = instance.filterOption({});
        const asserted = true;

        expect(actual).toBe(asserted);
      });

      test('not a case-insensitive substring of any label', () => {
        const { instance } = select();

        const actual = instance.filterOption({
          data: {
            label: 'myFooBar',
            alternativeLabels: [
              'myBazBar',
            ],
          },
        }, 'quux');
        const asserted = false;

        expect(actual).toBe(asserted);
      });
    });
  });
});
