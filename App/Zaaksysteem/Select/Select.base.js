import { Component } from 'react';
import { bind, callOrNothingAtAll } from '@mintlab/kitchen-sink';
import filterOption from './library/filterOption';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;
const MSG_CHOOSE = 'Select…';
const MSG_LOADING = 'Loading…';
const MSG_TYPE = 'Begin typing to search…';

export class SelectBase extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:choose': MSG_CHOOSE,
        'form:loading': MSG_LOADING,
        'form:beginTyping': MSG_TYPE,
      },
      disabled: false,
      autoLoad: false,
      filterOption,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
    this.timeoutId = null;
    bind(this,
      'handleBlur', 'handleChange', 'handleFocus', 'handleInputChange', 'handleKeyDown'
    );
  }

  // Lifecycle methods

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    if (this.props.autoLoad) {
      this.getChoices('');
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.choices) !== JSON.stringify(this.props.choices)) {
      this.setState({
        isLoading: false,
      });
    }
  }

  // Custom methods

  /**
   * Call form change function, if provided.
   *
   * @param {Object} value
   */
  handleChange(value) {
    const {
      onChange,
      name,
    } = this.props;

    callOrNothingAtAll(onChange, () => [{
      name,
      value,
      type: 'select',
    }]);
  }

  /**
   * Call form blur function, if provided.
   *
   * @param {Event} blurEvent
   */
  handleBlur(blurEvent) {
    const {
      props: {
        onBlur,
      },
    } = this;

    blurEvent.stopPropagation();
    callOrNothingAtAll(onBlur, () => []);
  }

  /**
   * Call form focus function, if provided
   *
   * @param {Event} focusEvent
   */
  handleFocus(focusEvent) {
    const {
      props: {
        onFocus,
        name,
      },
    } = this;

    focusEvent.stopPropagation();

    callOrNothingAtAll(onFocus, () => [{
      target: {
        name,
      },
    }]);
  }

  /**
  *
  * @param {Event} keyDownEvent
  */
  handleKeyDown(keyDownEvent) {
    const {
      props: {
        onKeyDown,
        name,
      },
    } = this;

    const {
      key,
      keyCode,
      charCode,
    } = keyDownEvent;

    keyDownEvent.stopPropagation();

    callOrNothingAtAll(onKeyDown, () => [{
      name,
      key,
      keyCode,
      charCode,
      type: 'select',
    }]);
  }

  /**
   * @param {string} input
   * @return {boolean}
   */
  isValidInput(input) {
    return Boolean(input) && (input.length >= MIN_CHARACTERS);
  }

  /**
   * @param {string} input
   */
  handleInputChange(input) {
    if (!this.isValidInput(input)) {
      return;
    }

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(() => {
      this.getChoices(input);
    }, DELAY);
  }

  /**
   * @param {*} input
   */
  getChoices(input) {
    const { getChoices } = this.props;

    if (!getChoices) {
      return;
    }

    this.setState({
      isLoading: true,
    });

    getChoices(input);
  }

}

export default SelectBase;
