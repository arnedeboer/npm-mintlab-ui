import { shallow } from 'enzyme';
import { createElement } from 'react';

/**
 * @param {number} count
 * @param {*} value
 * @return {Array}
 */
export const fill = (count, value) =>
  new Array(count).fill(value);

/**
 * @param {string} type
 * @param {Object} [target={}]
 * @return {Object}
 */
export const createEvent = (type, target = {}) => ({
  preventDefault: jest.fn(),
  stopPropagation: jest.fn(),
  target,
  type,
});

/**
 * @param {Arguments} argumentList
 * @return {Object}
 */
export const getShallowInstance = (...argumentList) =>
  shallow(createElement(...argumentList))
    .instance();

/**
 * @param {Arguments} argumentList
 * @return {Object}
 */
export function render(...argumentList) {
  const wrapper = shallow(createElement(...argumentList));
  const instance = wrapper.instance();
  const { props, state } = instance;

  return {
    instance,
    props,
    state,
  };
}

/**
 * @param {Function} spy
 * @param {number} count
 * @param {Array} argumentList
 */
export function spyOn(spy, count, ...argumentList) {
  expect(spy).toHaveBeenCalledTimes(count);
  expect(spy).toHaveBeenCalledWith(...argumentList);
}

/**
 * @param {Function} spy
 * @param {Array} sequence
 *   The last arguments the spy has been called with.
 */
export function spyOnSequence(spy, sequence) {
  const offset = spy.mock.calls.length - sequence.length;
  const increment = 1;

  for (const [index, value] of sequence.entries()) {
    expect(spy).toHaveBeenNthCalledWith((offset + index + increment), value);
  }
}
