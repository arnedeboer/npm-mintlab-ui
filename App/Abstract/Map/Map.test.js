import React from 'react';
import { shallow } from 'enzyme';
import Map from '.';

/**
 * @test {Map}
 */
describe('The `Map` component', () => {

  test('maps data to a fragment of components', () => {
    const list = [
      { label: 'foo' },
      { label: 'bar' },
    ];
    const Label = ({label}) => (
      <span>
        {label}
      </span>
    );
    const wrapper = shallow(
      <Map
        data={list}
        component={Label}
      />
    );
    const actual = wrapper.html();
    const expected = '<span>foo</span><span>bar</span>';

    expect(actual).toBe(expected);
  });

});
