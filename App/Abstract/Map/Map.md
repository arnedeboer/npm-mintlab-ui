# 🔌 `Map` component

> Declarative array to component mapper.

## See also

- [`Map` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Map)
- [`Map` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-map)
