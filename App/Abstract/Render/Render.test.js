import React from 'react';
import { shallow } from 'enzyme';
import Render from '.';

/**
 * @test {Render}
 */
describe('The `Render` component', () => {

  test('renders `null` if `condition` is false', () => {
    const wrapper = shallow(
      <Render
        condition={false}
      >Hello</Render>
    );
    const actual = wrapper.html();
    const expected = null;

    expect(actual).toBe(expected);
  });

  test('renders its child if `condition` is true', () => {
    const wrapper = shallow(
      <Render
        condition={true}
      >Hello</Render>
    );
    const actual = wrapper.text();
    const expected = 'Hello';

    expect(actual).toBe(expected);
  });

});
