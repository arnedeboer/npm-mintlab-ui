import { Component, createElement } from 'react';
import Loader from '../../Zaaksysteem/Loader';
import { bind, cloneWithout } from '@mintlab/kitchen-sink';

/**
 * Load components on demand with dynamic imports.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/LazyLoader
 * @see /npm-mintlab-ui/documentation/consumer/manual/LazyLoader.html
 *
 * @reactProps {Function} promise
 * @reactProps {Function} render
 */
export class LazyLoader extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      component: Loader,
    };
    bind(this, 'onResolved');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    const { promise } = this.props;

    promise()
      .then(this.extract)
      .then(this.onResolved);
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props,
      state: {
        component,
      },
    } = this;

    return createElement(component, cloneWithout(props, 'promise'));
  }

  // Custom methods

  /**
   * @param {Object} resolvedModule
   * @return {ReactElement}
   */
  extract(resolvedModule) {
    return resolvedModule.default;
  }

  /**
   * @param {Function} component
   */
  onResolved(component) {
    this.setState({
      component,
    });
  }
}

export default LazyLoader;
