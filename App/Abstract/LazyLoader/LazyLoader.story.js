import {
  React,
  stories,
} from '../../story';
import LazyLoader from '.';

stories(module, __dirname, {
  Default() {
    return (
      <LazyLoader
        promise={() => import('./story/Foo')}
        title="Get Out Of Your Lazy Bed!"
      />
    );
  },
});
