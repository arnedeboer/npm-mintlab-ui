import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { closeIndicatorStylesheet } from './CloseIndicator.style';
import Icon from '../../Material/Icon/Icon';
import IconButton from '@material-ui/core/IconButton';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} [props.action]
 * @return {ReactElement}
 */
const CloseIndicator = ({
  classes,
  action,
}) => (
  <IconButton
    onClick={action}
    color='inherit'
    classes={classes}
  >
    <Icon
      size='extraSmall'
    >close</Icon>
  </IconButton>
);

export default withStyles(closeIndicatorStylesheet)(CloseIndicator);
