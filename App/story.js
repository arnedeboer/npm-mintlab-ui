/**
 * **Storybook** utility functions.
 */
import React from 'react';
import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import { action, decorateAction } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { withState } from '@dump247/storybook-state';
import MaterialUiThemeProvider from './Material/MaterialUiThemeProvider';

export {
  React,
  action,
  boolean,
};

export {
  array,
  button,
  color,
  date,
  number,
  object,
  select,
  string,
  text,
} from '@storybook/addon-knobs';

const { keys } = Object;

const FIRST_INDEX = 0;

/**
 * **Storybook** utility function:
 * toggle the checkbox of a knob from a component prop function
 * (the knob is in another window).
 * @private
 * @param {string} id
 *   ID of the knob control. This corresponds to the first
 *   argument of the knob `boolean` method.
 * @param {Array} args
 *   Since the action decorator is just used to produce
 *   a side effect, arguments are just passed through.
 * @return {Array}
 */
export function toggleBooleanKnob(id, args) {
  const checkbox = window.parent.document.getElementById(id);

  if (checkbox) {
    checkbox.click();

    return [args, checkbox.checked];
  }

  return [args];
}

/**
 * **Storybook** utility function.
 * Prevent chained calls, e.g. closing the Material Drawer with the knob.
 * @todo Find out why that doesn't happen with the Dialog,
 * and merge this with `toggleBooleanKnob`/`toggleFactory`.
 * @private
 * @param {string} id
 * @return {Function}
 */
export function disableBooleanKnobFactory(id) {
  return function disableBooleanKnob() {
    const checkbox = window.parent.document.getElementById(id);

    if (checkbox && checkbox.checked) {
      checkbox.click();
    }
  };
}

/**
 * Storybook utility function.
 * @private
 * @param {Function} component
 * @param {string} id
 * @return {Function}
 */
export const toggleFactory = (component, id) => decorateAction([
  args => toggleBooleanKnob(id, args),
  ([args, bool]) => {
    if (args[FIRST_INDEX] instanceof Event) {
      return [args[FIRST_INDEX].type];
    }

    if (args.length) {
      return args;
    }

    return [id, bool];
  },
])(component.name);

/**
 * **Storybook** utility function: toggle a callback prop.
 * @private
 * @param {Function} component
 * @param {string} label
 * @return {Function|null}
 */
export function toggleHandler(component, label) {
  if (boolean(label, true)) {
    return event(component, label);
  }

  return null;
}

/**
 * @param {string} id
 * @param {Array} args
 * @return {Array}
 */
function getDomEventType(id, args) {
  if (id) {
    return [args[FIRST_INDEX].type, id];
  }

  return [args[FIRST_INDEX].type];
}

/**
 * **Storybook** utility function.
 * @todo refactor and document
 * @private
 * @param {Function} component
 * @param {string} [id]
 * @return {Array}
 */
export const event = (component, id) =>
  decorateAction([
    args => {
      if (args[FIRST_INDEX] instanceof Event) {
        return getDomEventType(id, args);
      }

      if (id) {
        return [id];
      }

      return args;
    },
  ])(component.name);

const DIRNAME_OFFSET = 1;
const SEPARATOR = '/';
const getPath = dirname => dirname
  .split(SEPARATOR)
  .slice(DIRNAME_OFFSET)
  .join(SEPARATOR);
const readme = require.context('.', true, /([A-Z][a-z]+)+\.md$/);

const hasReadme = path =>
  readme
    .keys()
    .includes(path);

/**
 * **Storybook** utility function.
 * @private
 * @param {Object} module
 * @param {string} __dirname
 * @param {Object} stories
 * @param {Object} [stores]
 */
/* eslint-disable */
export const stories = (module, __dirname, stories, stores) => {
  const dirname = getPath(__dirname);
  const [, filename] = dirname.split(SEPARATOR);
  const story = storiesOf(`${dirname}`, module);
  const README = `./${dirname}/${filename}.md`;

  if (hasReadme(README)) {
    story.addDecorator(withReadme(readme(README)));
  }

  story.addDecorator(story => (
    <MaterialUiThemeProvider>
      {story()}
    </MaterialUiThemeProvider>
  ));

  for (const key of keys(stories)) {
    const component = stories[key];

    if (stores && stores.hasOwnProperty(key)) {
      const state = stores[key];

      story.add(key, withState(state)(component));
    } else {
      story.add(key, component);
    }
  }
};

/**
 * **Storybook** utility component: generic icon
 * @private
 * @return {ReactElement}
 */
export const R2d2 = () => (
  <svg
    width="50"
    viewBox="0 0 512 512"
    fill="#ccc"
  >
    <path
      d="M469.213,415.183V199.845c0-19.814-16.075-35.89-35.89-35.89h-35.89v35.89v35.89v179.448l-35.89,89.725h143.559
         L469.213,415.183z"
    />
    <path
      d="M110.316,235.734v-35.89v-35.89h-35.89c-19.82,0-35.89,16.064-35.89,35.89v215.338l-35.89,89.725h143.559l-35.89-89.725
         V235.734z"
    />
    <path
      d="M378.893,146.01h0.596v-17.968c0-69.349-56.253-125.59-125.613-125.59c-69.361,0-125.614,56.241-125.614,125.59v17.968
         h0.596H378.893z M325.654,92.176c9.919,0,17.944,8.05,17.944,17.945c0,9.907-8.025,17.945-17.944,17.945
         s-17.945-8.038-17.945-17.945C307.709,100.226,315.735,92.176,325.654,92.176 M253.875,29.369c19.79,0,35.89,16.099,35.89,35.89
         c0,19.802-16.1,35.89-35.89,35.89c-19.838,0-35.89-16.087-35.89-35.89C217.985,45.468,234.037,29.369,253.875,29.369"
    />
    <path
      d="M379.488,163.955H128.261v251.228l35.89,35.89h179.448l35.89-35.89V163.955z M307.709,307.514H200.04
         c-9.918,0-17.945-8.025-17.945-17.944s8.026-17.945,17.945-17.945h107.669c9.919,0,17.945,8.026,17.945,17.945
         S317.628,307.514,307.709,307.514 M307.709,235.734H200.04c-9.918,0-17.945-8.038-17.945-17.945c0-9.907,8.026-17.945,17.945-17.945
         h107.669c9.919,0,17.945,8.038,17.945,17.945C325.654,227.697,317.628,235.734,307.709,235.734"
    />
  </svg>
);

/**
 * **Storybook** utility component: generic message
 * @private
 * @param {Object} props
 * @param {string} props.message
 * @return {ReactElement}
 */
export const StorybookMessage = ({ message }) => (
  <div
    style={{
      display: 'flex',
    }}
  >
    <div
      style={{
        marginRight: '5px',
      }}
    >
      <R2d2/>
    </div>
    <p
      dangerouslySetInnerHTML={{
        __html: message,
      }}
    />
  </div>
);

/**
 * **Storybook** utility component: knobs alert. Use if nothing is rendered
 * by default and Knobs interaction is required.
 * @private
 * @return {ReactElement}
 */
export const UseTheKnobs = () => (
  <StorybookMessage
    message="Use the <em>Knobs</em>, Luke!"
  />
);
