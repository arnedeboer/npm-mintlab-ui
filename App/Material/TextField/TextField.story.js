import {
  React,
  stories,
  text,
  boolean,
} from '../../story';
import FormTextField, { GenericTextField } from './TextField';
import Icon from '../../Material/Icon';

const stores = {
  'Form TextField': {
    value:'',
  },
  'Generic TextField': {
    value:'',
  },
};

/**
 * @param {SyntheticEvent} event 
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({
  target: {
    value,
  },
}, store) => store.set({ value });

stories(module, __dirname, {
  'Form TextField': function FormTextFieldStory({ store }) {

    const multiline = boolean('Multiline', false);
    const config = multiline ? { style: 'paragraph' } : {};

    return (
      <div 
        onChange={ event => onChange(event, store) }
      >
        <FormTextField
          info={text('Info', 'Type ahead')}
          label={text('Label', 'Text field')}
          error={text('Error', '')}
          required={true}
          hasFocus={boolean('Focus', false)}
          value={store.state.value}
          onChange={onChange}
          config={config}
        />
      </div>
    );
  },
  'Generic TextField': function GenericTextFieldStory({ store }) {

    const startAdornment = boolean('Start adornment', true) ? <Icon size='small'>search</Icon> : null;
    const closeAction = boolean('Close', true) ? () => {} : null;

    return (
      <div
        onChange={ event => onChange(event, store) }
      >
        <GenericTextField
          placeholder={text('Placeholder', 'Placeholder')}
          hasFocus={boolean('Focus', false)}
          value={store.state.value}
          onChange={onChange}
          startAdornment={startAdornment}
          closeAction={closeAction}
        />
      </div>
    );
  },
}, stores);
