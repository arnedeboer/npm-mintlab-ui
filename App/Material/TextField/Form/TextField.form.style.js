/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the TextField component
 * @param {Object} theme
 * @return {JSS}
 */
export const textFieldStyleSheet = ({
  palette: {
    error,
    text,
  },
  zIndex: {
    inputLabel,
  },
  mintlab: {
    greyscale,
    radius,
  },
}) => ({
  // Form Control
  formControl: {
    width: '100%',
  },
  // Form Helper
  formHelperError: {
    display: 'none',
  },
  // Input
  inputRoot: {
    '&&': {
      marginTop: 0,
    },
  },
  input: {
    padding: '20px 14px 10px 14px',
    borderRadius: radius.textField,
    backgroundColor: greyscale.light,
  },
  inputError: {
    backgroundColor: error.light,
  },
  // Label
  inputLabel: {
    zIndex: inputLabel,
    pointerEvents: 'none',
    left: '12px',
  },
  inputLabelShrink: {
    transform: 'translate(0, 16px)',
    fontSize: '9px',
  },
  inputLabelInactive: {
    top: '-6px',
  },
  inputLabelError: {
    '&&': {
      color: text.secondary,
    },
  },
});
