import { TextField } from '.';
import { getShallowInstance, spyOn } from '../../test';

const { assign } = Object;

const EMPTY = '';
const ANY = '*';
const ERROR = 'ERROR';
const classes = {
  inputClass: ANY,
  inputErrorClass: ERROR,
  formHelperErrorClass: ERROR,
  inputLabelClass: ANY,
  inputLabelShrinkClass: ANY,
  inputLabelInactiveClass: ANY,
  inputLabelErrorClass: ERROR,
  formControlClass: ANY,
};

const getInstance = props =>
  getShallowInstance(TextField, assign({
    classes: {},
  }, props));

/**
 * @test {TextField}
 */
xdescribe('The `TextField` component', () => {
  test('has a `handleFocus` method', () => {
    const spy = jest.fn();
    const instance = getInstance();

    instance.setState = spy;
    instance.handleFocus();

    const assertedCallCount = 1;
    const assertedArgument = {
      hasFocus: true,
    };

    spyOn(spy, assertedCallCount, assertedArgument);
  });

  test('has a `handleBlur` method', () => {
    const spy = jest.fn();
    const instance = getInstance();

    instance.setState = spy;
    instance.handleBlur();

    const assertedCallCount = 1;
    const assertedArgument = {
      hasFocus: false,
    };

    spyOn(spy, assertedCallCount, assertedArgument);
  });

  describe('has a `getStylingProps` method that', () => {
    test('sets no error classes if there is focus or no message', () => {
      const instance = getInstance();

      const actual = instance.getStylingProps({
        classes,
      });
      const asserted = {
        classes: {
          root: ANY,
        },
        InputProps: {
          classes: {
            input: ANY,
          },
          disableUnderline: false,
        },
        FormHelperTextProps: {
          classes: {
            root: EMPTY,
          },
        },
        InputLabelProps: {
          classes: {
            root: `${ANY} ${ANY}`,
            shrink: `${ANY} ${ANY}`,
          },
        },
      };

      expect(actual).toEqual(asserted);
    });

    test('sets error classes if there is no focus and a message', () => {
      const instance = getInstance();

      const actual = instance.getStylingProps({
        classes,
        error: ERROR,
        hasFocus: false,
      });
      const asserted = {
        classes: {
          root: ANY,
        },
        InputProps: {
          classes: {
            input: `${ANY} ${ERROR}`,
          },
          disableUnderline: true,
        },
        FormHelperTextProps: {
          classes: {
            root: ERROR,
          },
        },
        InputLabelProps: {
          classes: {
            root: `${ANY} ${ANY} ${ERROR}`,
            shrink: `${ANY} ${ANY} ${ERROR}`,
          },
        },
      };

      expect(actual).toEqual(asserted);
    });
  });
});
