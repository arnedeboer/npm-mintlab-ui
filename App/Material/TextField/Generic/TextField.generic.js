import React, { Component, createElement } from 'react';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { unique } from '@mintlab/kitchen-sink';
import { textFieldStyleSheet } from './TextField.generic.style';
import classNames from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import { bind } from '@mintlab/kitchen-sink';

/**
 *  *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * This is a simple TextField with generic styling,
 * meant to be used as a general GUI component.
 *
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {string} placeholder
 * @reactProps {string} name
 * @reactProps {ReactElement} [startAdornment]
 * @reactProps {Function} [closeAction]
 * @reactProps {string} value
 * @reactProps {boolean} hasFocus
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    bind(this, 'EndAdornment', 'StartAdornment');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        classes: {
          input,
          formControl,
          focus,
        },
        name,
        value,
        hasFocus,
        startAdornment,
        placeholder,
        closeAction: action,
      },
      StartAdornment,
      EndAdornment,
    } = this;

    return (
      <MuiTextField
        value={value}
        id={unique()}
        name={name}
        placeholder={placeholder}
        classes={{
          root: classNames(formControl, { [focus]: hasFocus }),
        }}
        InputProps={{
          classes: {
            input: classNames(input, { [focus]: hasFocus }),
          },
          disableUnderline: true,
          ...(startAdornment && { startAdornment: StartAdornment() }),
          ...(action && { endAdornment: EndAdornment() }),
        }}
      />
    );
  }

  /**
   * @return {ReactElement}
   */
  StartAdornment() {
    const {
      classes: {
        focus,
      },
      hasFocus,
      startAdornment,
    } = this.props;

    const props = {
      position: 'start',
      classes: {
        root: classNames({ [focus]: hasFocus }),
      },
    };

    return createElement(InputAdornment, props, startAdornment);
  }

  /**
   * @return {ReactElement}
   */
  EndAdornment() {
    const {
      closeAction: action,
      classes: {
        focus,
      },
      hasFocus,
    } = this.props;

    const closeButton = createElement(CloseIndicator, { action });
    const props = {
      position: 'end',
      classes: {
        root: classNames({ [focus]: hasFocus }),
      },
    };

    return createElement(InputAdornment, props, closeButton);
  }

}

export default withStyles(textFieldStyleSheet)(TextField);
