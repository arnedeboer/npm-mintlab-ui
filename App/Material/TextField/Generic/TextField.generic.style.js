/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the TextField component
 * @param {Object} theme
 * @return {JSS}
 */
export const textFieldStyleSheet = ({
  palette: {
    primary,
  },
  mintlab: {
    greyscale,
    radius,
  },
  typography,
}) => ({
  // Form Control
  formControl: {
    width: '100%',
    padding: '0px 6px 0px 12px',
    'background-color': greyscale.light,
    'border-radius': radius.genericInput,
    'justify-content': 'center',
    height: '36px',
    fontWeight: typography.fontWeightLight,
  },
  // Input
  input: {
    color: greyscale.offBlack,
    marginRight: '6px',
  },
  focus: {
    color: primary.dark,
    'background-color': primary.light,
  },
});
