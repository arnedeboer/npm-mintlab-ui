# 🔌 `MaterialUiThemeProvider` component

> Theme provider for wrapping the consumer's component tree root.

## Example

    <MaterialUiThemeProvider>
      <App/>
    </MaterialUiThemeProvider>

## See also

- [`MaterialUiThemeProvider` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-materialuithemeprovider)

## External resources

 - [*Material-UI* Theme Provider](https://material-ui.com/customization/themes/)
 