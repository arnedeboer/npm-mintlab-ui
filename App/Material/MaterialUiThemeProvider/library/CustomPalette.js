import { createElement } from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { overrides, palette } from './theme';

const {
  MuiButton: {
    root,
    fab,
    label,
  },
} = overrides;

const {
  common: {
    white,
  },
  error,
  review,
} = palette;

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root,
      label,
      fab,
      containedPrimary: {
        color: white,
      },
    },
  },
  palette: {
    primary: review,
    secondary: error,
  },
  typography: {
    useNextVariants: true,
  },
});

export const CustomPalette = ({
  children,
}) => createElement(MuiThemeProvider, {
  theme,
  children,
});
