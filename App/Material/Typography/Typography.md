# 🔌 Typography components

## Exports

This module exposes named exports that correspond 
with the *Material Design* Type Scale:

- `H1`
- `H2`
- `H3`
- `H4`
- `H5`
- `H6`
- `Subtitle1`
- `Subtitle2`
- `Body1`
- `Body2`
- `Caption`
- `Overline`

## See also

- [Typography stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Typography)
- [Typography API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-typography-style)

## External resources

- [*Material Foundation*: Typography: Type Scale](https://material.io/design/typography/the-type-system.html#type-scale) 
- [*Material-UI* `Typography` API](https://material-ui.com/api/typography/)
