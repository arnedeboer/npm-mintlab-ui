import {
  React,
  stories,
  text,
} from '../../story';
import {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  Subtitle1,
  Subtitle2,
  Body1,
  Body2,
  Caption,
  Overline,
} from '.';
import { withTypography } from './library/withTypography';

const Base = ({
  classes: {
    subtitle1,
    overline,
  },
}) => (
  <div>
    <span
      className={subtitle1}
    >The quick brown fox</span>
    {' '}
    <span
      className={overline}
    >jumps over the lazy dog.</span>
  </div>
);

stories(module, __dirname, {
  H1() {
    return (
      <H1>
        {text('Greeting', 'Hello, world!')}
      </H1>
    );
  },
  H2() {
    return (
      <H2>
        {text('Greeting', 'Hello, world!')}
      </H2>
    );
  },
  H3() {
    return (
      <H3>
        {text('Greeting', 'Hello, world!')}
      </H3>
    );
  },
  H4() {
    return (
      <H4>
        {text('Greeting', 'Hello, world!')}
      </H4>
    );
  },
  H5() {
    return (
      <H5>
        {text('Greeting', 'Hello, world!')}
      </H5>
    );
  },
  H6() {
    return (
      <H6>
        {text('Greeting', 'Hello, world!')}
      </H6>
    );
  },
  Subtitle1() {
    return (
      <Subtitle1>
        {text('Greeting', 'Hello, world!')}
      </Subtitle1>
    );
  },
  Subtitle2() {
    return (
      <Subtitle2>
        {text('Greeting', 'Hello, world!')}
      </Subtitle2>
    );
  },
  Body1() {
    return (
      <Body1>
        {text('Greeting', 'Hello, world!')}
      </Body1>
    );
  },
  Body2() {
    return (
      <Body2>
        {text('Greeting', 'Hello, world!')}
      </Body2>
    );
  },
  Caption() {
    return (
      <Caption>
        {text('Greeting', 'Hello, world!')}
      </Caption>
    );
  },
  Overline() {
    return (
      <Overline>
        {text('Greeting', 'Hello, world!')}
      </Overline>
    );
  },
  'With Typography': function enhanceWithTypography() {
    return React.createElement(withTypography(Base));
  },
});
