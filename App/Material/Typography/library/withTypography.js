import { withStyles } from '@material-ui/core/styles';

const { keys } = Object;

export const whitelist = [
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'subtitle1',
  'subtitle2',
  'body1',
  'body2',
  'button',
  'caption',
  'overline',
];

/**
 * @param {Object} theme
 * @param {Object} theme.typography
 * @return {Object}
 */
const getTypography = ({ typography }) => {
  function map(accumulator, key) {
    if (whitelist.includes(key)) {
      accumulator[key] = typography[key];
    }

    return accumulator;
  }

  return keys(typography)
    .reduce(map, {});
};

const enhance = withStyles(getTypography);

export const withTypography = Component =>
  enhance(Component);
