import { createElement } from 'react';
import { shallow } from 'enzyme';
import { withTypography, whitelist } from './withTypography';

const { keys } = Object;

describe('The `withTypography` function', () => {
  test('passes a `classes` prop with all typographic classes', () => {
    const Foo = () => createElement('div');
    const wrapper = shallow(createElement(withTypography(Foo)));

    const actual = keys(wrapper.props().classes).sort();
    const expected = whitelist.sort();

    expect(actual).toEqual(expected);
  });
});
