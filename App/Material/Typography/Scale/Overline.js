import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Overline
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Overline = ({
  children,
  classes,
}) => (
  <Typography
    variant="overline"
    classes={classes}
  >{children}</Typography>
);
