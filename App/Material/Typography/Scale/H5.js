import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=H5
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const H5 = ({
  children,
  classes,
}) => (
  <Typography
    variant="h5"
    classes={classes}
  >{children}</Typography>
);
