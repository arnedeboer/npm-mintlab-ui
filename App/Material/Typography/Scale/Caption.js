import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Caption
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Caption = ({
  children,
  classes,
}) => (
  <Typography
    variant="caption"
    classes={classes}
  >{children}</Typography>
);
