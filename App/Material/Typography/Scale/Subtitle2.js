import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Subtitle2
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Subtitle2 = ({
  children,
  classes,
}) => (
  <Typography
    variant="subtitle2"
    classes={classes}
  >{children}</Typography>
);
