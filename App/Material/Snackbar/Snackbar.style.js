/**
 * Style Sheet for the Snackbar component
 * @return {JSS}
 */
export const snackbarStyleSheet = ({
  mintlab: {
    greyscale,
    radius,
  },
}) => ({
  root: {
    padding: '0 16px 0 24px',
    borderRadius: radius.snackbar,
    minHeight: '50px',
  },
  iconButton: {
    width: '36px',
    height: '36px',
    padding: 0,
    '&:hover': {
      backgroundColor: `${greyscale.lighter}33`,
    },
  },
});
