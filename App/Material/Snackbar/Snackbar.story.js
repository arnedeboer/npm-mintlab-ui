import {
  React,
  stories,
  text,
  number,
} from '../../story';
import Snackbar from '.';

const SNACKBAR_TIMEOUT = 6000;

stories(module, __dirname, {
  Default() {
    return (
      <Snackbar
        message={text('Message', 'Snackbar message')}
        autoHideDuration={number('Auto hide duration', SNACKBAR_TIMEOUT)}
      />
    );
  },
});
