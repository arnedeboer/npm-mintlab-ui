import {
  React,
  stories,
  text,
} from '../../story';
import RadioGroup from '.';

stories(module, __dirname, {
  Default() {
    const choices = [
      {
        label: 'Foo',
        value: 'FOO',
      },
      {
        label: 'Bar',
        value: 'BAR',
      },
    ];

    return (
      <RadioGroup
        choices={choices}
        error={text('Error', '')}
        label="Multiple choice"
        name="foo"
        value="BAR"
      />
    );
  },
});
