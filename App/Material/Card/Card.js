import React from 'react';
import MuiCard from '@material-ui/core/Card';
import MuiCardContent from '@material-ui/core/CardContent';
import MuiCardHeader from '@material-ui/core/CardHeader';
import Render from '../../Abstract/Render';
import { cardStylesheet } from './Card.style';
import { withStyles } from '@material-ui/core/styles';

/**
 * *Material Design* **Card**.
 * - facade for *Material-UI* `Card`, `CardHeader` and `CardContent`
 * - children are rendered in the `CardContent`,
 *   additional props are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Card
 * @see /npm-mintlab-ui/documentation/consumer/manual/Card.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {ReactElement} props.children
 * @param {string} props.description
 * @param {string} props.title
 * @return {ReactElement}
 */
export const Card = props => {
  const {
    classes,
    children,
    title,
    description,
    ...rest
  } = props;

  return (
    <MuiCard
      classes={{
        root: classes.card,
      }}
      {...rest}
    >
      <Render condition={title || description}>
        <MuiCardHeader
          classes={{
            root: classes.header,
            title: classes.title,
            subheader: classes.subheader,
          }}
          title={title}
          subheader={description}
        />
      </Render>

      <MuiCardContent
        classes={{
          root: classes.content,
        }}
      >
        { children }
      </MuiCardContent>
    </MuiCard>
  );
};

export default withStyles(cardStylesheet)(Card);
