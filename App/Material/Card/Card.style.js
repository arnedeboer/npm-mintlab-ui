const { assign } = Object;

/**
 * Style Sheet for the {@link Card} Card component
 * @param {Object} theme
 * @return {JSS}
 */
export const cardStylesheet = ({
  mintlab: {
    shadows,
    radius,
    greyscale,
  },
  typography,
}) => ({
  card: {
    overflow: 'visible',
    '& + $card': {
      marginTop: '25px',
    },
    boxShadow: shadows.flat,
    borderRadius: radius.card,
  },
  header: {
    paddingBottom: 0,
  },
  title: typography.h4,
  subheader: assign({}, typography.body2, {
    color: greyscale.evenDarker,
    fontWeight: typography.fontWeightLight,
  }),
  content: typography.body2,
});
