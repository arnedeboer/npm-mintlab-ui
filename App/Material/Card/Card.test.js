import React from 'react';
import { mount } from 'enzyme';
import Card from '.';

const getMountedCard = props => (
  mount(
    <Card
      {...props}
    >
      <div>Hello, World!</div>
    </Card>
  )
);

/**
 * @test {Card}
 */
xdescribe('The `Card` component', () => {
  test('renders the title prop in a `CardHeader` component', () => {
    const wrapper = getMountedCard({
      title: 'Hello!',
    });

    const actual = wrapper.find('Render CardHeader').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('renders the description prop in a `CardHeader` component', () => {
    const wrapper = getMountedCard({
      description: 'Hello!',
    });

    const actual = wrapper.find('Render CardHeader').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('does not render the `CardHeader` component when there is no title or description prop', () => {
    const wrapper = getMountedCard();
    const actual = wrapper.find('Render CardHeader').length;
    const asserted = 0;

    expect(actual).toBe(asserted);
  });
});
