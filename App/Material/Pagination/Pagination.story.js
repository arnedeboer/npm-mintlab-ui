import {
  React,
  stories,
} from '../../story';
import MuiTable from '@material-ui/core/Table';
import MuiTableBody from '@material-ui/core/TableBody';
import MuiTableRow from '@material-ui/core/TableRow';
import Pagination from '.';

/* eslint-disable no-magic-numbers */
const stores = {
  Default: {
    count: 101,
    page: 0,
    rowsPerPage: 10,
  },
};

stories(module, __dirname, {
  Default({ store }) {
    function changeRowsPerPage(event) {
      store.set({
        page: 0,
        rowsPerPage: event.target.value,
      });
    }

    function getNewPage(page) {
      store.set({
        page,
      });
    }

    return (
      <MuiTable>
        <MuiTableBody>
          <MuiTableRow>
            <Pagination
              page={store.state.page}
              count={store.state.count}
              labelRowsPerPage={'Per pagina:'}
              rowsPerPageOptions={[5, 10, 25, 50, 100]}
              rowsPerPage={store.state.rowsPerPage}
              changeRowsPerPage={changeRowsPerPage}
              labelDisplayedRows={({ count, from, to }) => `${from}–${to} van ${count}`}
              getNewPage={getNewPage}
            />
          </MuiTableRow>
        </MuiTableBody>
      </MuiTable>
    );
  },
}, stores);
