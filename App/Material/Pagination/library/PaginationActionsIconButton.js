import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { pageExists } from './functions';

/**
 * *Material UI* `IconButton` facade component for pagination.
 *
 * @param {Object} props
 * @param {Object} props.button
 * @param {Number} props.button.destination
 * @param {Function} props.button.icon
 * @param {Object} props.button.buttonClasses
 * @param {Number} props.page
 * @param {Number} props.pageCount
 * @param {Function} props.onChangePage
 * @return {ReactElement}
 */
export const PaginationActionsIconButton = ({
  button: {
    destination,
    icon,
    buttonClasses,
  },
  onChangePage,
  page,
  pageCount,
}) => (
  <IconButton
    classes={buttonClasses}
    disabled={!pageExists(destination, pageCount) || destination === page}
    onClick={() => {
      onChangePage(destination);
    }}
  >{icon}</IconButton>
);

export default PaginationActionsIconButton;
