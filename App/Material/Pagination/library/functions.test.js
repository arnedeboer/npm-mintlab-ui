import { createElement } from 'react';
import Icon from '../../Icon';
import {
  getButtonConfig,
  getPageButtons,
  pageExists,
  symmetricallyIncrementByOneFromZeroUpTo,
} from './functions';

/* eslint-disable no-magic-numbers */

describe('The `PaginationActionsSection` component', () => {
  describe('has a `pageExists` method that checks whether a page exists if it ', () => {
    test('exists within a range of pages', () => {
      expect(pageExists(3, 5)).toBe(true);
    });

    test('is beyond a range of pages', () => {
      expect(pageExists(8, 5)).toBe(false);
    });

    test('is negative', () => {
      expect(pageExists(-1, 5)).toBe(false);
    });

    test('is the first possible page', () => {
      expect(pageExists(0, 5)).toBe(true);
    });
  });

  describe('has a `symmetricallyIncrementByOneFromZeroUpTo` method', () => {
    test('creates array with incremental integers from negative integer to positive integer', () => {
      const actual = symmetricallyIncrementByOneFromZeroUpTo(2);
      const asserted = [-2, -1, 0, 1, 2];

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `getPageButtons` method', () => {
    test('creates an array of buttonConfiguration for the pageJumpButtons with all pages avaiable', () => {
      const currentPage = 5;
      const numberOfPages = 10;
      const actual = getPageButtons(currentPage, numberOfPages);
      const asserted = [
        {
          destination: 3,
          icon: 4,
          rootClass: 'pageJump',
        },
        {
          destination: 4,
          icon: 5,
          rootClass: 'pageJump',
        },
        {
          destination: 5,
          icon: 6,
          rootClass: 'pageJump',
        },
        {
          destination: 6,
          icon: 7,
          rootClass: 'pageJump',
        },
        {
          destination: 7,
          icon: 8,
          rootClass: 'pageJump',
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test([
      'creates an array of button configurations for the page jump buttons',
      'with leading pages and trailing pages not avaiable',
    ].join(' '), () => {
      const currentPage = 1;
      const numberOfPages = 3;
      const actual = getPageButtons(currentPage, numberOfPages);
      const asserted = [
        {
          destination: 0,
          icon: 1,
          rootClass: 'pageJump',
        },
        {
          destination: 1,
          icon: 2,
          rootClass: 'pageJump',
        },
        {
          destination: 2,
          icon: 3,
          rootClass: 'pageJump',
        },
      ];

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `getButtonConfig` method that creates an', () => {
    const pageButtonClasses = {
      paginatorButton: 'paginatorButton',
      smallStep: 'smallStep',
      bigStep: 'bigStep',
      pageJump: 'pageJump',
      currentPage: 'currentPage',
      disabled: 'disabled',
    };

    test('array with button configurations for the `stepBackButtons` section', () => {
      const page = 3;
      const pageCount = 10;
      const actual = getButtonConfig('stepBackButtons', page, pageCount, pageButtonClasses);
      const asserted = [
        {
          destination: 2,
          icon: createElement(Icon, null, 'navigate_before'),
          rootClass: 'smallStep',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton smallStep',
          },
        },
        {
          destination: -7,
          icon: '-10',
          rootClass: 'bigStep',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton bigStep',
          },
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test('array with button configurations for the `stepForwardButtons` section', () => {
      const page = 3;
      const pageCount = 10;
      const actual = getButtonConfig('stepForwardButtons', page, pageCount, pageButtonClasses);
      const asserted = [
        {
          destination: 13,
          icon: '+10',
          rootClass: 'bigStep',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton bigStep',
          },
        },
        {
          destination: 4,
          icon: createElement(Icon, null, 'navigate_next'),
          rootClass: 'smallStep',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton smallStep',
          },
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test('array with button configurations for the `pageJumpButtons` section with all pages available', () => {
      const page = 5;
      const pageCount = 10;
      const actual = getButtonConfig('pageJumpButtons', page, pageCount, pageButtonClasses);
      const asserted = [
        {
          destination: 3,
          icon: 4,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 4,
          icon: 5,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 5,
          icon: 6,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'currentPage',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 6,
          icon: 7,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 7,
          icon: 8,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test([
      'array with button configurations for the `pageJumpButtons` section',
      'with leading pages and trailing pages not avaiable',
    ].join(''), () => {
      const page = 1;
      const pageCount = 3;
      const actual = getButtonConfig('pageJumpButtons', page, pageCount, pageButtonClasses);
      const asserted = [
        {
          destination: 0,
          icon: 1,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 1,
          icon: 2,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'currentPage',
            root: 'paginatorButton pageJump',
          },
        },
        {
          destination: 2,
          icon: 3,
          rootClass: 'pageJump',
          buttonClasses: {
            disabled: 'disabled',
            root: 'paginatorButton pageJump',
          },
        },
      ];

      expect(actual).toEqual(asserted);
    });
  });
});
