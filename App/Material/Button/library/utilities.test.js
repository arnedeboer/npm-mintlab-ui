import {
  oneOf,
  parseAction,
  parsePresets,
  colors,
  sizes,
  variants,
} from './utilities';

describe('The `Button`  component’s `utility` module', () => {
  describe('exports a `oneOf` function that', () => {
    test('returns a mutually exlcusive intersection value of two arrays', () => {
      const actual = oneOf(['foo', 'bar'], ['foo', 'baz']);
      const expected = 'foo';

      expect(actual).toBe(expected);
    });

    test('returns undefined if no intersection is found', () => {
      const actual = oneOf(['foo', 'bar'], ['quux', 'baz']);
      const expected = undefined;

      expect(actual).toBe(expected);
    });

    test('returns undefined if more than one intersection is found', () => {
      const actual = oneOf(['foo', 'bar'], ['foo', 'bar']);
      const expected = undefined;

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `parseAction` function', () => {
    test('that sets the `href` property if its argument is a string', () => {
      const href = 'FOOBAR';
      const actual = parseAction(href);
      const expected = {
        href,
      };

      expect(actual).toEqual(expected);
    });

    test('that sets the `onClick` property if its argument is a function', () => {
      const onClick = () => {};
      const actual = parseAction(onClick);
      const expected = {
        onClick,
      };

      expect(actual).toEqual(expected);
    });

    describe('exports a `parsePresets` function that', () => {
      test('returns an object with its properties set to `undefined` or `false` if no known presets are given', () => {
        const actual = parsePresets(['hello', 'world']);
        const expected = {
          color: undefined,
          fullWidth: false,
          mini: false,
          size: undefined,
          variant: undefined,
        };

        expect(actual).toEqual(expected);
      });

      test('returns an object with the `mini` property set to `true` if it is in the presets', () => {
        const actual = parsePresets(['mini']).mini;
        const expected = true;

        expect(actual).toBe(expected);
      });

      test('returns an object with the `fullWidth` property set to `true` if it is in the presets', () => {
        const actual = parsePresets(['fullWidth']).fullWidth;
        const expected = true;

        expect(actual).toBe(expected);
      });

      describe('supports the colors', () => {
        for (const color of colors) {
          test(`${color}`, () => {
            const actual = parsePresets([color]).color;
            const expected = color;

            expect(actual).toBe(expected);
          });
        }
      });

      describe('supports the sizes', () => {
        for (const size of sizes) {
          test(`${size}`, () => {
            const actual = parsePresets([size]).size;
            const expected = size;

            expect(actual).toBe(expected);
          });
        }
      });

      describe('supports the variants', () => {
        for (const variant of variants) {
          test(`${variant}`, () => {
            const actual = parsePresets([variant]).variant;
            const expected = variant;

            expect(actual).toBe(expected);
          });
        }
      });
    });
  });
});
