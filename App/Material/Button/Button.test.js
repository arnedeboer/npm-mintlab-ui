import React from 'react';
import { mount } from 'enzyme';
import Button from '.';

jest.mock('../Icon', () => 'span');

/**
 * @test {Button}
 */
describe('The `Button` component', () => {
  test('renders the default color if there is no color preset', () => {
    const wrapper = mount(
      <Button
        presets={[]}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().color;
    const expected = 'default';

    expect(actual).toBe(expected);
  });

  test('renders the primary color if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['primary']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().color;
    const expected = 'primary';

    expect(actual).toBe(expected);
  });

  test('renders the secondary color if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['secondary']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().color;
    const expected = 'secondary';

    expect(actual).toBe(expected);
  });

  test('renders the medium size if there is no size preset', () => {
    const wrapper = mount(
      <Button
        presets={[]}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().size;
    const expected = 'medium';

    expect(actual).toBe(expected);
  });

  test('renders the small size if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['small']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().size;
    const expected = 'small';

    expect(actual).toBe(expected);
  });

  test('renders the large size if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['large']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().size;
    const expected = 'large';

    expect(actual).toBe(expected);
  });

  test('renders the text variant if there is no variant preset', () => {
    const wrapper = mount(
      <Button
        presets={[]}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().variant;
    const expected = 'text';

    expect(actual).toBe(expected);
  });

  test('renders the text variant if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['text']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().variant;
    const expected = 'text';

    expect(actual).toBe(expected);
  });

  test('renders the `contained` variant if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['contained']}
      >Hello!</Button>
    );
    const actual = wrapper.find('Button Button').props().variant;
    const expected = 'contained';

    expect(actual).toBe(expected);
  });

  test('renders the `fab` variant if it is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['fab']}
      >add</Button>
    );
    const actual = wrapper.find('Button Button').props().variant;
    const expected = 'fab';

    expect(actual).toBe(expected);
  });

  test('renders an icon button if icon is in the presets', () => {
    const wrapper = mount(
      <Button
        presets={['icon']}
      >add</Button>
    );
    const actual = wrapper.find('Button IconButton').length;
    const expected = 1;

    expect(actual).toBe(expected);
  });
});
