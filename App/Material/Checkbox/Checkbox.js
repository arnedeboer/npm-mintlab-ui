import React from 'react';
import MuiCheckbox from '@material-ui/core/Checkbox';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';

/**
 * *Material Design* **Checkbox** selection control.
 * - facade for *Material-UI* `Checkbox`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Checkbox
 * @see /npm-mintlab-ui/documentation/consumer/manual/Checkbox.html
 *
 * @param {Object} props
 * @param {boolean} [props.checked=false]
 * @param {boolean} [props.disabled=false]
 * @param {string} [props.label]
 * @param {string} [props.name]
 * @param {string} [props.color='primary']
 * @return {ReactElement}
 */
export const Checkbox = ({
  checked=false,
  disabled,
  label,
  name,
  color='primary',
}) => (
  <MuiFormControlLabel
    control={(
      <MuiCheckbox
        checked={checked}
        disabled={disabled}
        name={name}
        color={color}
      />
    )}
    label={label}
  />
);

export default Checkbox;
