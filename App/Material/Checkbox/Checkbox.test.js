import React from 'react';
import { mount } from 'enzyme';
import Checkbox from '.';

/**
 * @test {Checkbox}
 */
xdescribe('The `Checkbox` component', () => {
  test('passes its shared props to the wrapped component', () => {
    const props = {
      color: 'secondary',
      defaultChecked: true,
      disabled: true,
      name: 'MyFancyCheckbox',
    };
    const wrapper = mount(
      <Checkbox {...props}/>
    );
    const actual = wrapper
      .find('Checkbox Checkbox')
      .props();
    const expected = expect.objectContaining(props);

    expect(actual).toEqual(expected);
  });
});
