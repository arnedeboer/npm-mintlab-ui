# 🔌 `Icon` component

> *Material Design* **Icon**.

## Example

    <Icon
      color="red" 
      name="menu"
    />

## See also

- [`Icon` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Icon)
- [`Icon` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-dialog)

## External resources

- [*Material Icons*](https://material.io/tools/icons/)
- [*Material Foundation*: Iconography: System icons](https://material.io/design/iconography/system-icons.html)
- [*Material-UI* `Icon` API](https://material-ui.com/api/icon/)
