import {
  React,
  stories,
  select,
} from '../../story';
import Icon from '.';
import * as icons from './library/';

const { keys } = Object;
const colors = [
  'inherit',
  'primary',
  'secondary',
  'disabled',
  'error',
];

stories(module, __dirname, {
  Default() {
    const color = select('Color', colors, 'inherit');
    const size = select('Size', [
      'extraSmall',
      'small',
      'medium',
      'large',
    ], 'medium');
    const identifiers = keys(icons);

    return (
      <div>
        {identifiers.map((name, index) => (
          <span
            key={index}
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              margin: '0.2em 0.5em',
              padding: '0.2em 0.5em',
              border: '1px solid #ddd',
              fontFamily: 'Menlo, Consolas',
              fontSize: '0.8rem',
            }}
          >
            <Icon
              color={color}
              size={size}
            >{name}</Icon>
            <span
              style={{
                marginLeft: '0.5em',
                color: '#666',
              }}
            >
              {name}
            </span>
          </span>
        ))}
      </div>
    );
  },
});
