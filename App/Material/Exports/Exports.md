# 🔌 `Exports`

> Serves as a passthrough for Material-UI modules. See `webpack/library/entry.js`.
