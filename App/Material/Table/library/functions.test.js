import { getCellClass } from './functions';

/* eslint-disable no-magic-numbers */

const bucket = {
  tableCell: 'foo',
  firstTableCell: 'bar',
  lastTableCell: 'quux',
};

describe('The `getCellClass` function', () => {
  test('sets all class names for a single element', () => {
    const actual = getCellClass([''], bucket, 0);
    const expected = 'foo bar quux';

    expect(actual).toEqual(expected);
  });

  test('sets the class name for the first of several elements', () => {
    const actual = getCellClass(['', ''], bucket, 0);
    const expected = 'foo bar';

    expect(actual).toEqual(expected);
  });

  test('sets the class name for the last of several elements', () => {
    const actual = getCellClass(['', ''], bucket, 1);
    const expected = 'foo quux';

    expect(actual).toEqual(expected);
  });

  test('sets only the generic class name for an element that is not first or last', () => {
    const actual = getCellClass(['', '', ''], bucket, 1);
    const expected = 'foo';

    expect(actual).toEqual(expected);
  });
});
