import classNames from 'classnames';

/**
 * @param {number} index
 * @return {boolean}
 */
export function isFirstIndex(index) {
  const firstIndex = 0;

  return index === firstIndex;
}

/**
 * @param {number} index
 * @param {Array} array
 * @return {number}
 */
export function isLastIndex(index, array) {
  const offset = 1;
  const lastIndex = (array.length - offset);

  return (index === lastIndex);
}

/**
 * @param {string} columnName
 * @param {Object} classes
 * @return {string}
 */
const getSpecificCellClass = (columnName, classes) =>
  classes[`${columnName}Cell`] || classes.otherCells;

/**
 * @param {Array} columns
 * @param {Object} classes
 * @param {number} key
 * @return {string}
 */
export const getCellClass = (columns, classes, key) => {
  const classList = [classes.tableCell];
  const columnName = columns[key].name;

  if (isFirstIndex(key)) {
    classList.push(classes.firstTableCell);
  }

  if (isLastIndex(key, columns)) {
    classList.push(classes.lastTableCell);
  }

  return classNames(...classList, getSpecificCellClass(columnName, classes));
};
