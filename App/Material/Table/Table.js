import React from 'react';
import MuiTable from '@material-ui/core/Table';
import MuiTableHead from '@material-ui/core/TableHead';
import MuiTableBody from '@material-ui/core/TableBody';
import MuiTableFooter from '@material-ui/core/TableFooter';
import MuiTableRow from '@material-ui/core/TableRow';
import MuiTableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';
import Render from '../../Abstract/Render';
import TableBody from './TableBody';
import { tableStyleSheet } from './Table.style';

/**
 * *Material Design* **Table**.
 * - facade for *Material-UI* `Table`, `TableHead`,
 *   `TableBody`, `TableFooter`, `TableRow` and `TableCell`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Table
 * @see /npm-mintlab-ui/documentation/consumer/manual/Table.html
 *
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @reactProps {Array} columns
 * @reactProps {Array} rows
 * @reactProps {string} noResultDescription
 * @return {ReactElement}
 */
const Table = ({
  children,
  classes,
  columns,
  rows,
  noResultDescription,
}) => (
  <MuiTable className={classes.table}>
    <MuiTableHead>
      <MuiTableRow>
        {columns.map(({ label }, index) => (
          <MuiTableCell
            className={classes.tableHeadCell}
            key={index}
          >{label}</MuiTableCell>
        ))}
      </MuiTableRow>
    </MuiTableHead>
    <MuiTableBody>
      <TableBody
        classes={classes}
        columns={columns}
        rows={rows}
        noResultDescription={noResultDescription}
      />
    </MuiTableBody>
    <Render
      condition={children}
    >
      <MuiTableFooter>
        <MuiTableRow>
          {children}
        </MuiTableRow>
      </MuiTableFooter>
    </Render>
  </MuiTable>
);

export default withStyles(tableStyleSheet)(Table);
