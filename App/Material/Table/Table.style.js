/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Table component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const tableStyleSheet = ({
  palette: {
    primary,
    common,
  },
  mintlab: {
    greyscale,
    radius,
  },
}) => ({
  table: {},
  tableHeadCell: {
    fontWeight: 'bold',
    color: greyscale.black,
    fontSize: '16px',
    whiteSpace: 'nowrap',
    paddingLeft: '20px',
    paddingRight: '0',
    borderBottom: `1px solid ${common.white}`,
  },
  tableRow: {
    borderTop: `1px solid ${greyscale.dark}`,
    '&:hover': {
      background: primary.lighter,
      borderTop: `1px solid ${common.white}`,
    },
    '&:hover + tr': {
      borderTop: `1px solid ${common.white}`,
    },
  },
  tableCell: {
    padding: '20px 0px 20px 20px',
    borderBottom: 'none',
  },
  firstTableCell: {
    borderTopLeftRadius: radius.tableRow,
    borderBottomLeftRadius: radius.tableRow,
  },
  lastTableCell: {
    borderTopRightRadius: radius.tableRow,
    borderBottomRightRadius: radius.tableRow,
  },
  noResult: {
    textAlign: 'center',
    fontSize: '30px',
    borderBottom: `1px solid ${common.white}`,
  },
});
