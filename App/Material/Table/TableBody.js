import React from 'react';
import MuiTableRow from '@material-ui/core/TableRow';
import MuiTableCell from '@material-ui/core/TableCell';
import Loader from '../../Zaaksysteem/Loader';
import { getCellClass } from './library/functions';

/**
 * *Material UI* `TableBody` facade component for table.
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Array} props.columns
 * @param {Array} props.rows
 * @param {string} props.noResultDescription
 * @return {ReactElement}
 */
export const TableBody = ({
  classes,
  columns,
  rows,
  noResultDescription,
}) => {
  if (rows) {
    if(rows.length) {
      return rows.map((row, rowIndex) => (
        <MuiTableRow
          key={rowIndex}
          className={classes.tableRow}
        >
          {columns.map(({ name }, columnIndex) => (
            <MuiTableCell
              key={columnIndex}
              className={getCellClass(columns, classes, columnIndex)}
            >{row[name]}</MuiTableCell>
          ))}
        </MuiTableRow>
      ));
    }

    return (
      <MuiTableRow>
        <MuiTableCell
          colSpan={columns.length}
          className={classes.noResult}
        >
          {noResultDescription}
        </MuiTableCell>
      </MuiTableRow>
    );
  }

  const firstIndex = 0;

  return (
    <MuiTableRow>
      <MuiTableCell
        colSpan={columns.length}
        className={getCellClass(columns, classes, firstIndex)}
      >
        <Loader/>
      </MuiTableCell>
    </MuiTableRow>
  );
};

export default TableBody;
