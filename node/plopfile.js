const { resolve } = require('path');
const camelCase = require('lodash.camelcase');
const upperFirst = require('lodash.upperfirst');
const glob = require('glob');
const { red, yellow } = require('chalk');

const EXIT_CODE_ERROR = 1;
const { assign } = Object;

const toConstructor = name => upperFirst(camelCase(name));

const getErrorMessage = (needle, haystack) =>
  `${red(needle)} already exists in ${yellow(haystack)}`;

/**
 * Return the mutated parameter with a normalized component name
 * or exit if the component already exists in the App tree.
 *
 * @param {Object} data
 * @return {Object}
 */
function resolveComponent(data) {
  const component = toConstructor(data.component);
  const [flatMatch] = [
    ...glob.sync(`App/*/${component}`),
    ...glob.sync(`App/Material/Typography/style/${component}\.js`),
  ];

  if (flatMatch) {
    const [, categoryName] = flatMatch.split('/');

    console.error(getErrorMessage(component, categoryName));
    process.exit(EXIT_CODE_ERROR);
  }

  // `data` is mutated on purpose.
  return assign(data, {
    component,
  });
}

module.exports = function plop({ setGenerator }) {
  setGenerator('Component', {
    description: 'Component',
    prompts: [
      {
        type: 'input',
        name: 'component',
        message: 'Component name:',
      },
      {
        type: 'input',
        name: 'synopsis',
        message: 'Component description:',
        validate(input) {
          if (/^[^ ]*( [^ ]+){2,}/.test(input)) {
            return true;
          }

          return 'A description is required';
        },
      },
      {
        type: 'list',
        name: 'category',
        message: 'Component category:',
        default: 'Zaaksysteem',
        choices: [
          {
            name: 'Abstract',
            value: 'Abstract',
          },
          {
            name: 'Material',
            value: 'Material',
          },
          {
            name: 'Zaaksysteem',
            value: 'Zaaksysteem',
          },
        ],
      },
      {
        type: 'list',
        name: 'type',
        message: 'Component type:',
        default: 'component',
        choices: [
          {
            name: 'Stateless Functional Component',
            value: 'component',
          },
          {
            name: 'Class Component',
            value: 'component-class',
          },
        ],
      },
    ],
    actions(data) {
      const { category, component, type } = resolveComponent(data);
      const componentDirectory = resolve('App', category, component);

      data.fragment = [category, component]
        .join('-')
        .toLowerCase();

      return [
        {
          type: 'add',
          path: `${componentDirectory}/${component}.js`,
          templateFile: `plop/${type}.hbs`,
        },
        {
          type: 'add',
          path: `${componentDirectory}/${component}.story.js`,
          templateFile: 'plop/story.hbs',
        },
        {
          type: 'add',
          path: `${componentDirectory}/${component}.test.js`,
          templateFile: 'plop/test.hbs',
        },
        {
          type: 'add',
          path: `${componentDirectory}/${component}.md`,
          templateFile: 'plop/readme.hbs',
        },
        {
          type: 'add',
          path: `${componentDirectory}/package.json`,
          templateFile: 'plop/package.hbs',
        },
      ];
    },
  });
};
