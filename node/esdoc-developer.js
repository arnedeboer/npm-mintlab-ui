// ESDoc configuration for developer API documentation
const glob = require('glob');
const del = require('del');

const lowerCamelCase = '[a-z][A-Za-z0-9]+';
const COVER = '/cover.md';
const manualGlob = `./node/manual/developer/*.md`;
const files = glob
  .sync(manualGlob)
  .filter(path => !path.endsWith(COVER));
const { npm_package_config_PUBLIC } = process.env;
const destination = `./${npm_package_config_PUBLIC}/documentation/developer`;

del.sync(destination);

module.exports = {
  source: './App',
  destination,
  includes: [
    'esdoc.js',
    `(^|/)${lowerCamelCase}\\.js$`,
  ],
  excludes: [
    '^index\\.js$',
    '/([A-Z][a-z0-9]+|library)/index\\.js$',
    '/style/[A-Za-z0-9]+\\.js$',
    '/Form/library/typemap\\.js$',
    '/style/[A-Za-z0-9]+\\.js$',
    '/library/with[A-Z][A-Za-z0-9]+\\.js$',
  ],
  plugins: [
    {
      name: 'esdoc-standard-plugin',
      option: {
        brand: {
          title: '@mintlab/ui'
        },
        test: {
          source: './App',
          interfaces: [
            'describe',
            'test',
          ],
          includes: [
            `/${lowerCamelCase}\\.test\\.js$`
          ]
        },
        manual: {
          globalIndex: true,
          index: './node/manual/developer/cover.md',
          files,
        },
      },
    },
    {
      name: 'esdoc-ecmascript-proposal-plugin',
      option: {
        dynamicImport: true,
        objectRestSpread: true
      },
    },
    {
      name: 'esdoc-jsx-plugin',
    },
  ],
};
