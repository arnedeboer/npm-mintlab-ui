# Workflow

## Prerequisites

1. your git `origin` is a clone of `zaaksysteem/npm-mintlab-ui`

## Proposing changes

1. Create a local branch for any set of changes that will be published
2. Commit changes to your branch using the
   [Angular Convention commit message format](https://www.npmjs.com/package/conventional-changelog-angular#commit-message-format)
3. Verify the pipeline will work:
   `$ npm run dry`
4. Push your changes to your origin
5. Create a merge request for the `master` branch of 
   `zaaksysteem/npm-mintlab-ui`

## Merging changes

1. Review the code changes.
2. Review the commit messages. If necessary, edit the commit message 
   of the merge request using the 
   [Angular Convention commit message format](https://www.npmjs.com/package/conventional-changelog-angular#commit-message-format)
   to add any information that is missing for the release.
   In particular, flag any breaking changes.
3. Merge. If all is well, a new version of @mintlab/ui will be released.
   You can check that after the pipeline is done with `npm info @mintlab/ui`.

## See also

[Conventional Commits specification](https://www.conventionalcommits.org/)
