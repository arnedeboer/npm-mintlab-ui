# Third party dependencies

- [*React*](https://reactjs.org/docs/react-api.html)
- [*Material-UI*](https://material-ui.com/)

## Configuration and infrastructure

- [Storybook](https://storybook.js.org/basics/introduction/)
- [webpack configuration](https://webpack.js.org/configuration/)
    - cf. `./storybook/webpack`
- [Jest configuration](http://facebook.github.io/jest/docs/en/configuration.html)    
- [PLOP](https://plopjs.com/)
