# Known issues

- Inspecting a component with the React developer tools for
  Google Chrome by clicking on it does not work 
  (switching from the `Elements` pane to the `React` pane
  usually works, though). That seems to be related to 
  Storybook internals.

### FAQ

**Q: What *should* actually be published?**

A: With the excpetions of the `README`, `LICENSE` and `package.json` files,
nothing that's under version control (cf. `.gitignore` and `.npmignore`):

    - `distribution/ui.js` (`main` in `package.json`)
    - `distribution/ui.css`  

## Misc.

- https://github.com/babel/babel-eslint/issues/530
