const { join } = require('path');

module.exports = {
  ui: join(process.cwd(), 'App', 'index.js'),
};
