const {
  DefinePlugin,
  optimize: {
    UglifyJsPlugin,
  },
} = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const VisualizerPlugin = require('webpack-visualizer-plugin');
const { join } = require('path');
const { npm_package_config_PUBLIC } = process.env;

module.exports = [
  new DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"',
    },
  }),
  new UglifyJsPlugin({
    comments: false,
    sourceMap: false,
  }),
  new ExtractTextPlugin('[name].css'),
  new OptimizeCssAssetsPlugin({
    cssProcessorOptions: {
      safe: true,
    },
  }),
  new VisualizerPlugin({
    filename: join('..', npm_package_config_PUBLIC, 'webpack', 'visualizer.html'),
  }),
];
