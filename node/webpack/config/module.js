const { join } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const includePaths = [
  join(process.cwd(), 'App'),
];
const { assign } = Object;

const cssLoader = options => ({
  loader: 'css-loader',
  options: assign(options, {
    importLoaders: 1,
    sourceMap: false,
  }),
});

const postCssLoader = {
  loader: 'postcss-loader',
  options: {
    plugins: [
      require('postcss-import')(),
      require('postcss-preset-env')(),
    ],
    sourceMap: false,
  },
};

module.exports = {
  rules: [
    {
      exclude: /\/node_modules\//,
      test: /\.jsx?$/,
      use: [
        'babel-loader',
      ],
    },
    {
      test: /\.css$/,
      include: includePaths,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          cssLoader({
            modules: true,
          }),
          postCssLoader,
        ],
      }),
    },
    {
      test: /\.css$/,
      include: /\/node_modules\//,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          cssLoader({
            modules: false,
          }),
          postCssLoader,
        ],
      }),
    },
    {
      test: /\.woff2?$/,
      use: [
        {
          loader: 'file-loader',
        },
      ],
    },
    {
      test: /\.svg$/,
      use: {
        loader: 'svg-url-loader',
      },
    },
  ],
};
