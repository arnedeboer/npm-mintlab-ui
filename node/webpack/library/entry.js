/**
 * Generate the main file of the published package.
 * All public modules are named exports.
 */
const { writeFileSync } = require('fs');
const glob = require('glob');
const { parse } = require('path');

const {
  env: {
    npm_package_config_WEBPACK_PUBLIC_PATH: publicPath,
  },
} = process;

const APP = './App';
const GLOB = './*/*/!(*.*).js';

// The webpack `publicPath` configuration depends on the consuming application.
// It can be configured in package.json#config, but not after the build.
// Cf. https://webpack.js.org/guides/public-path/#on-the-fly
const BOILERPLATE = `
// GENERATED ENTRY POINT FILE FOR THE WEBPACK PUBLICATION BUILD, DO NOT EDIT.
__webpack_public_path__ = '${publicPath}';
`.trim();

const JOIN = '\n';
const DIR_SLICE_INDEX = -1;
const exportAll = ['Exports', 'Typography', 'TextField'];

function getModulePath(filePath) {
  const { dir, name } = parse(filePath);

  if (exportAll.includes(name)) {
    return `export * from '${dir}';`;
  }

  const [last] = dir
    .split('/')
    .slice(DIR_SLICE_INDEX);

  if (name !== last) {
    return `export {default as ${name}} from '${dir}/${name}';`;
  }

  return `export {default as ${name}} from '${dir}';`;
}

const files = glob
  .sync(GLOB, {
    cwd: APP,
  })
  .map(fileName => getModulePath(fileName));

const output = [
  BOILERPLATE,
  files.join(JOIN),
].join(JOIN);

writeFileSync(`${APP}/index.js`, output);
