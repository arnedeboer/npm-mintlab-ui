# Storybook configuration

## ECMAScript code style

As all code in this directory is executed in the Node.js
runtime environment, you **must** use
[Node modules](https://nodejs.org/api/modules.html)
instead of ECMAScript 2015 `import`/`export`.
