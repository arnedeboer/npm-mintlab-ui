require('@storybook/addons');
require('@storybook/addon-options/register');
require('@storybook/addon-knobs/register');
require('@storybook/addon-actions/register');
require('@storybook/addon-a11y/register');
require('storybook-readme/register');
require('@dump247/storybook-state/register');
