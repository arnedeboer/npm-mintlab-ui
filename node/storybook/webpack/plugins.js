const cherrypack = require('@mintlab/cherrypack');

module.exports = cherrypack({
  development() {
    return [];
  },
  production() {
    const {
      optimize: {
        UglifyJsPlugin,
      },
    } = require('webpack');

    return [
      new UglifyJsPlugin({
        comments: false,
        sourceMap: false,
      }),
    ];
  },
});
