const cherrypack = require('@mintlab/cherrypack');

module.exports = cherrypack({
  development: 'cheap-module-source-map',
  production: undefined,
});
